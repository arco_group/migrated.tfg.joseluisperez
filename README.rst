GEVA - Generador de Eventos para un entorno de Virtualidad Aumentada
====================================================================

La finalidad de este proyecto es representar eventos generados en una ciudad real en su correspondencia virtual.


Requisitos
----------

Debido a que las instrucciones que aparecen a continuación son para distribuciones GNU/Linux, se recomienda usar Debian o cualquiera de sus derivadas para obtener una mejor experiencia.

Antes de proceder a la instalación de la plataforma, son necesarios los siguientes requisitos:

* aptitude
* build-essential
* linux-headers
* python-dev
* python-pip
* libffi-dev
* libssl-dev
* virtualbox
* ansible
* vagrant

Por tanto, basta con abrir un terminal y escribir lo siguiente::

  $ sudo apt install aptitude build-essential linux-headers python-dev python-pip libffi-dev libssl-dev virtualbox ansible vagrant

|

Instalando la plataforma
------------------------

El primer paso para instalar la plataforma **GEVA** es ejecutar los siguientes comandos: 

Para iniciar la máquina virtual mediante Vagrant y aprovisionarla con Ansible::

  $ vagrant up

Una vez terminado el paso anterior, accedemos a la máquina::

  $ vagrant ssh

Ahora debemos cambiar el password del usuario **ubuntu** para poder acceder mediante el entorno gráfico xfce4::

  $ sudo passwd ubuntu

A continuación, presionamos CTRL^D para volver a nuestra máquina nativa y ejecutamos lo siguiente para apagar la máquina virtual::

  $ vagrant halt

Para activar la interfaz gráfica, es necesario modificar el archivo **Vagrantfile**, descomentando las siguientes líneas::

  config.vm.provider :virtualbox do |vb|
    vb.gui = true
  end

Tras realizar, todos los pasos anteriores, basta con arrancar de nuevo la máquina virtual y acceder con el usuario ubuntu y la contraseña que hayamos introducido anteriormente. Para poder ejecutar el programa que analiza una cámara o bien representar en el mundo virtual distintos eventos generados mediante clientes ZeroC Ice, es necesario iniciar primero xfce::
  
  $ sudo startxfce4

Por último, para tener establecer el teclado español, hay que seguir estos pasos:

Ir a Applications > Settings > Keyboard. Una vez estemos en ese menú, hay que ir a la pestaña "Layout" y desmarcar la opción "Use system defaults". Por último, basta con sustituir el teclado inglés por el español pulsando "Edit". Hay que buscar el idioma "Spanish", desplegar ese idioma para ver las distintas opciones y seleccionar ésta: "spanish (include dead tilde)". Tras ésto, ya se puede empezar a utilizar la plataforma.

Detección de objetos mediante OpenCV
------------------------------------

Para analizar un vídeo con el fin de detectar los objetos en movimiento que aparezcan en él, debemos acceder a la carpeta **/home/ubuntu/geva/3.2.0/** e introducir el siguiente comando para realizar el procesamiento del vídeo deseado (habiendo situado dicho vídeo en la carpeta ****/home/ubuntu/geva/videos**)::

  # python objects-detector.py --video ../videos/<nombre del vídeo que se quiere procesar>.mp4

Cabe indicar que aunque esta explicación sea para un vídeo previamente grabado, el algoritmo también permite procesar directamente el vídeo
obtenido en tiempo real mediante una cámara o una webcam, por lo que puede usarse por ejemplo junto con una Raspberry Pi.

Aunque el formato del vídeo no tiene por qué ser mp4, se recomienda encarecidamente hacer uso de él puesto que es el que se ha utilizado durante el desarrollo del proyecto.


Representación de eventos en el mundo virtual de forma distribuida mediante el Blender Game Engine
---------------------------------------------------------------------------------

Para la representación de eventos generados en un mundo virtual previamente creado, se ha desarrollado un Servidor ZeroC Ice que implementa una API cuyas operaciones permiten realizar diferentes acciones como, por ejemplo, crear o borrar objetos, cambiar el color, pintar una ruta, etc. Para ejecutar el servidor, es necesario abrir el archivo .blend que contiene el mundo virtual sobre el que se representarán los eventos. Para ello, hay que ir al directorio **/home/ubuntu/geva/blender/src/visualizer**::

  # blender model.blend

Una vez abierto, hacemos click con el botón derecho del ratón en el script que aparece a la derecha y seleccionamos la opción "run script".

Con el fin de probar las distintas operaciones que conforman la API, se ha implementado un cliente ZeroC Ice que realiza invocaciones a cada una de esas funciones. Para ejecutar el cliente, hay que ir al directorio **/home/ubuntu/geva/blender/src/realcamera** y ejecutar el siguiente comando::

# python Camera.py "visualizer1 -t -e 1.1:tcp -h <ip del servidor> -p <puerto del servidor>"

por defecto, puede indicarse localhost como ip del servidor (ya que éste y el cliente se encuentran en la misma máquina) y como puerto el 9090 (aunque puede cambirse en el archivo ). Por tanto, el comando sería el siguiente::

  # python Camera.py "visualizer1 -t -e 1.1:tcp -h localhost -p 9090"


Cabe indicar que el archivo **Camera.py** del cliente implementa ejemplos de prueba para las operaciones definidas por el servidor y que son invocables por los clientes. Para probar una de ellas, basta con descomentar la llamada a la función que se desea probar e introducir el comando antes mencionado para ejecutar el cliente. Además, para poder ejecutar el cliente primero hay que ejecutar el servidor como se ha descrito anteriormente.

Una vez se haya utilizado la plataforma, para apagar la máquina virtual, podemos ejecutar este comando desde la máquina virtual con xfce4:

# shutdown -h now

o bien este otro desde la consola que hemos realizado la operación vagrant up:

$ vagrant halt

