#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017


# import the necessary packages
import argparse
from datetime import datetime
import imutils
import time
import cv2
from conf import *
import os
import operator
import json
from classes import *

data = {}
data['objects'] = []


def addVehicle(x,y):
	#Divide vehicles according to the lane at which they are situated
	
	if (y >= 100 and y <= 110): #down
		lane = 1
	else: #up
		lane = 0

	vehicle = Vehicle(lane, x)
	vehicles.append(vehicle)

def sortVehiclesByXCoord():	
	vehicles.sort(key=operator.attrgetter("xCoord"), reverse=False) 
	

def sendDataToFile(t):

	#frameObject = Frame(vehiclesPerFrame,t)
	#pickle.dump(frameObject,dataFile)
	
	data['objects'].append({
		'class':'Frame',
		'vehiclesPerFrame': vehiclesPerFrame,
    	'time':t
	})
	#jsonData = json.dumps(data)
	#json.dump(jsonData, dataFile)
	#jsons.append(jsonData)
	for vehicle in vehicles:
		data['objects'].append({
			"class":"Vehicle",
			"lane": vehicle.lane,
    		"xCoord":vehicle.xCoord
		})
		#vehicle.time = t
		#pickle.dump(vehicle,dataFile)
		
		#jsonData = json.dumps(data)
		#jsons.append(jsonData)
		#json.dump(jsonData, dataFile)
	del vehicles[:]

	
vehicles = []
maxVehiclesInAFrame = 0 #maximum number of objects in Blender that I will need to draw the cars

os.system("rm *.txt")

confObj = setup()

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="path to the video file")
args = vars(ap.parse_args())

# if the video argument is None, then we are reading from webcam
if args.get("video", None) is None:
	camera = cv2.VideoCapture(0)
	time.sleep(0.25)

# otherwise, we are reading from a video file
else:
	camera = cv2.VideoCapture(args["video"])

# initialize the first frame in the video stream
firstFrame = None
avg = None
text = "Detecting objects..."

roadDataFile = open('roadData.txt','w')
dataFile = open('data.json','w')

# loop over the frames of the video
while True:
	# grab the current frame 
	(grabbed, frame) = camera.read()

	# if the frame could not be grabbed, then we have reached the end
	# of the video
	if not grabbed:
		break

	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

 	#if the average frame is None, initialize it
	if avg is None:
		print ("[INFO] starting background model...")
		avg = gray.copy().astype("float")
		continue

	# accumulate the weighted average between the current frame and
	# previous frames, then compute the difference between the current
	# frame and running average
	cv2.accumulateWeighted(gray, avg, 0.5)
	frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

	# threshold the delta image, dilate the thresholded image to fill
	# in holes, then find contours on thresholded image
	thresh = cv2.threshold(frameDelta, getDeltaThresh(confObj), 255,
		cv2.THRESH_BINARY)[1]
	thresh = cv2.dilate(thresh, None, iterations=2)
	(_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	

	vehiclesPerFrame = 0
	
# loop over the contours
	for c in cnts:
		
		# if the contour is too small, ignore it
		
		if cv2.contourArea(c) < getMinArea(confObj):
			continue
		else:
			vehiclesPerFrame = vehiclesPerFrame + 1
			# compute the bounding box for the contour, draw it on the frame,
			# and update the text
			(x, y, w, h) = cv2.boundingRect(c)

			cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
		
			addVehicle(x,y)

	if len(vehicles) > 0:
		t = time.time()
		sortVehiclesByXCoord()
		sendDataToFile(t)
	

	if (vehiclesPerFrame > maxVehiclesInAFrame):
		maxVehiclesInAFrame = vehiclesPerFrame

	# draw the text and timestamp on the frame
	ts = time.strftime("%A %d %B %Y %I:%M:%S%p")
	cv2.putText(frame, "Status: {}".format(text), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
	cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
		0.35, (0, 255, 0), 1)

	# show the frame and record if the user presses a key
	cv2.imshow("Real video", frame)
	if getShowThreshWindow(confObj) == "true":
		cv2.imshow("Thresh", thresh)
	if getShowFrameDeltaWindow(confObj) == "true":
		cv2.imshow("Frame Delta", frameDelta)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break


roadDataFile.write(str(maxVehiclesInAFrame)+','+str(getNumberOfLanes(confObj)))
json.dump(data, dataFile) 


# cleanup the camera and close any open windows. Close dataFile and roadDataFile too.
camera.release()
cv2.destroyAllWindows()
dataFile.close()
roadDataFile.close()
