#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017

class Frame:

   def __init__(self, time, nVehicles):
      self.time = time
      self.nVehicles = nVehicles

   def getFrameData(self):
      return self.time + ',' + self.nVehicles

class Vehicle:

   def __init__(self, lane, xCoord):
      self.lane = lane
      self.xCoord = xCoord

   def getVehicleData(self):
      return self.lane + ',' + self.xCoord

class Pedestrian:

   def __init__(self, lane, xCoord):
      self.xCoord = xCoord

   def getPedestrianData(self):
      return self.xCoord
