#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017

import configparser

def setup(conffile="../conf/detection.cfg"):
    configuration = configparser.ConfigParser()
    configuration.read(conffile)
    return configuration

def getDeltaThresh(configobj):
	return int(configobj.get('detection_parameters', 'delta_thresh'))

def getMinArea(configobj):
	return int(configobj.get('detection_parameters', 'min_area'))

def getShowThreshWindow(configobj):
	return configobj.get('detection_parameters', 'show_thresh_window')	

def getShowFrameDeltaWindow(configobj):
	return configobj.get('detection_parameters', 'show_frame_delta_window')

def getNumberOfLanes(configobj):
	return int(configobj.get('road_data', 'lanes'))   
