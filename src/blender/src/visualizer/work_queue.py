#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017

from threading import Thread
from queue import Queue

class WorkQueue:
	QUIT = 'QUIT'
	CANCEL = 'CANCEL'

	def __init__(self):
		self.queue = Queue()

	def step(self):
		if self.queue.empty():
			return

		job = self.queue.get()
		if job is not self.QUIT:
			job.execute()
			self.queue.task_done()
			return

		self.queue.task_done()
		self.queue.put(self.CANCEL)

		for job in iter(self.queue.get(), self.CANCEL):
			job.cancel()
			self.queue.task_done()

		self.queue.task_done()

	def add(self, cb, func, *args):
		self.queue.put(Job(cb, func, args))

	def destroy(self):
		self.queue.put(self.QUIT)
		self.queue.join()

class Job(object):
	def __init__(self, cb, func, args):
		self.cb = cb
		self.func = func
		self.args = args

	def execute(self):
		result = self.func(*self.args)
		if self.cb:
			self.cb.ice_response(result)

	def cancel(self):
		pass
		#self.cb.ice_exception(Example.RequestCancelException())
