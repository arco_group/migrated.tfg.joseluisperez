#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017

import sys
import bpy
import Ice
Ice.loadSlice('geva.ice')
from work_queue import WorkQueue
import geva
import mathutils
from math import radians
import bmesh
import time

class visualizerI(geva.visualizer):
	def __init__(self, work_queue):
		print ("visualizer servant init..")
		self.work_queue = work_queue
		self.context = bpy.context
		self.scene = self.context.scene
		self.data = bpy.data
		self.ops = bpy.ops
		self.colors = {}
		self.colors['red'] = (1,0,0)
		self.colors['yellow'] = (1,1,0)
		self.colors['blue'] = (0,1,3)
		self.colors['green'] = (0,1,0) 
		self.colors['black'] = (0,0,0)
		self.colors['white'] = (2,2,2)
		self.colors['orange'] = (8,1,0)
		self.colors['violet'] = (1,0,8)
		

	def __getAllObjects(self):
		return self.data.objects

	def __getObjectByIdentifier(self, identifier):
		return self.data.objects[identifier]

	def __destroyAllObjects(self):

		objects = self.__getAllObjects()
		removableObjects = [ob for ob in objects if ob.layers[0]]
		for o in removableObjects:
			if o.name in objects:
				objects.remove(objects[o.name], True)
		self.scene.update()
		bpy.context.area.tag_redraw() 

	def __setObjectVisibility(self, identifier, visibility):
		obj = self.__getObjectByIdentifier(identifier)
		obj.hide = visibility
		self.scene.update()	

	def __destroyObjectByIdentifier(self, identifier):
		obj = self.__getObjectByIdentifier(identifier)
		self.__setObjectVisibility(identifier, True)		
		objects = self.__getAllObjects()
		objects.remove(obj, True)
		self.scene.update()		

	def __drawObject(self, obj, identifier, location):
		newObj = obj.copy()
		newObj.data = obj.data.copy()
		newObj.animation_data_clear()
		newObj.name = identifier		
		self.scene.objects.link(newObj)
		self.__setObjectLocation(newObj, location)
		self.scene.update()

	def __setObjectLocation(self, obj, location):
		obj.location = (location.x,location.y,location.z)

	def __setColor(self, obj, color):
		if color.lower() in self.colors:
			newMaterial = bpy.data.materials.new(name="Material")
			obj.data.materials.append(newMaterial)		 
			obj.active_material.diffuse_color = self.colors[color.lower()]

	def __moveObject(self, obj, location):
		obj.location = obj.location + mathutils.Vector((location.x-obj.location.x,location.y-obj.location.y,location.z-obj.location.z))
		self.scene.update()
		time.sleep(0.07)
	
	def __setObjectRotation(self, obj, angles):
		obj.rotation_euler = (radians(angles.x), radians(angles.y), radians(angles.z))

	def __scaleObject(self, obj, scale):
		obj.scale = (scale.x,scale.y,scale.z)

	def __paintRoute(self, identifier, color, verts):
		faces = []

		for i in range(0,len(verts)-1):
			faces.append((i,i+1,i+1))
		mesh_data = bpy.data.meshes.new("")
		mesh_data.from_pydata(verts, [], faces)
		mesh_data.update()
		obj = bpy.data.objects.new(identifier, mesh_data)
		scene = bpy.context.scene
		scene.objects.link(obj)
		obj.select = True
		bpy.context.scene.objects.active = obj		
		me = obj.data
		bm = bmesh.new()
		bm.from_mesh(me)
		faces = bm.faces[:]

		for face in faces:
			r = bmesh.ops.extrude_discrete_faces(bm, faces=[face])
			bmesh.ops.translate(bm, vec=mathutils.Vector((0,2.1,0)), verts=r['faces'][0].verts)

		bm.to_mesh(me)
		me.update()
		self.__setColor(obj, color)
		bpy.context.scene.update()

	#MIRAR SI SE PUEDE HACER Y SI NO QUITARLO
	def __joinObjects(self, objs):
		pass

	def destroyAllObjects(self, cb, current=None):
		self.__destroyAllObjects()

	def scaleObject(self, cb, identifier, scale, current=None):
		self.__scaleObject(self.__getObjectByIdentifier(identifier), scale)

	def setObjectVisibility(self, cb,  identifier, visibility, current=None):
		self.__setObjectVisibility(identifier, visibility)

	def setObjectColor(self, cb, identifier, color, current=None):
		self.__setColor(self.__getObjectByIdentifier(identifier), color)

	def destroyObject(self, cb, identifier, current=None):
		self.__destroyObjectByIdentifier(identifier)

	def setObjectRotation(self, cb, identifier, angles, current=None):
		obj = self.__getObjectByIdentifier(identifier)
		self.__setObjectRotation(obj, angles)

	def setObjectLocation(self, cb, identifier, location, current=None):
		obj = self.__getObjectByIdentifier(identifier)
		self.__setObjectLocation(obj, location)	

	def moveObject(self, cb, identifier, location, current=None):
		self.__moveObject(self.__getObjectByIdentifier(identifier), location)

	#MIRAR SI SE PUEDE HACER Y SI NO QUITARLO
	def joinObjects(self, cb, identifiers, current=None):
		objs = []
		for i in identifiers:			
			obj = self.__getObjectByIdentifier(i)
			objs.append(obj)
		self.__joinObjects(objs)

	def paintDetectedEventByCoords(self, cb, identifier, prototype, location, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, location)

	def paintDetectedEventByLocationName(self, cb, identifier, prototype, locationName, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, self.__getObjectByIdentifier(locationName).location)

	def paintVehicleByCoords(self, cb, identifier, prototype, location, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, location)	

	def paintVehicleByLocationName(self, cb, identifier, prototype, locationName, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, self.__getObjectByIdentifier(locationName).location)	

	def paintPersonByCoords(self, cb, identifier, prototype, location, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, location)

	def paintPersonByLocationName(self, cb, identifier, prototype, locationName, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, self.__getObjectByIdentifier(locationName).location)	 

	def paintRouteByCoords(self, cb, identifier, route, color, current=None):
		verts = []
		
		while len(route) > 0:
			v = route.pop()
			verts.append((v.x,v.y,v.z))

		self.__paintRoute(identifier, color, verts)

	def paintRouteByNames(self, cb, identifier, route, color, current=None):
		verts = []
		
		while len(route) > 0:
			objectName = route.pop()
			obj = self.__getObjectByIdentifier(objectName)
			verts.append((obj.location.x,obj.location.y,obj.location.z))

		self.__paintRoute(identifier, color, verts)
	
	def paintServiceByCoords(self, cb, identifier, prototype, location, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, location)

	def paintServiceByLocationName(self, cb, identifier, prototype, locationName, current=None):
		obj = self.__getObjectByIdentifier(prototype)
		self.__drawObject(obj, identifier, self.__getObjectByIdentifier(locationName).location)

	def paintCircularAreaByCoords(self, cb, identifier, color, location, radius, current=None):
		obj = self.__getObjectByIdentifier("Area")
		self.__drawObject(obj, identifier, location)
		obj = self.__getObjectByIdentifier(identifier)
		self.__setColor(obj, color)
		self.__scaleObject(obj, geva.Scale(radius,radius,0.001))

	def paintCircularAreaByLocationName(self, cb, identifier, color, locationName, radius, current=None):
		obj = self.__getObjectByIdentifier("Area")
		self.__drawObject(obj, identifier, self.__getObjectByIdentifier(locationName).location)
		obj = self.__getObjectByIdentifier(identifier)
		self.__setColor(obj, color)
		self.__scaleObject(obj, geva.Scale(radius,radius,0.001))

	def paintDesiredAreaByCoords(self, cb, identifier, color, location, vertices, current=None):
		bm = bmesh.new()

		for v in vertices:
			bm.verts.new((v.x, v.y, v.z))

		bm.faces.new(bm.verts)
		bm.normal_update()
		me = self.data.meshes.new("")
		bm.to_mesh(me)
		newObj =self.data.objects.new("", me)
		newObj.animation_data_clear()
		newObj.name = identifier	
		self.scene.objects.link(newObj)
		self.scene.update()
		self.__setObjectLocation(newObj, location)	
		self.__setColor(newObj, color)

	def paintDesiredAreaByLocationName(self, cb, identifier, color, locationName, vertices, current=None):
		bm = bmesh.new()

		for v in vertices:
			bm.verts.new((v.x, v.y, v.z))

		bm.faces.new(bm.verts)
		bm.normal_update()
		me = self.data.meshes.new("")
		bm.to_mesh(me)
		newObj =self.data.objects.new("", me)
		newObj.animation_data_clear()
		newObj.name = identifier	#MIRAR SI SE PUEDE HACER CON CHANGEOBJECTIDENTIFIER
		self.scene.objects.link(newObj)
		self.scene.update()
		self.__setObjectLocation(newObj, self.__getObjectByIdentifier(locationName).location)	
		self.__setColor(newObj, color)

	def paintSensorByCoords(self, cb, identifier, prototype, location, coverage, current=None):
		pass

	def paintSensorByLocationName(self, cb, identifier, prototype, locationName, coverage, current=None):
		pass

	def accessSensor(self, cb, identifier, current=None):
		pass

	#IMPLEMENTAR
	def setActiveCamera(self, cb, identifier, current=None):
		pass

	def changeObjectIdentifier(self, cb, oldIdentifier, newIdentifier, current=None):
		obj = self.__getObjectByIdentifier(oldIdentifier)
		obj.name = newIdentifier

	def eventGenerator(self, cb, identifier, vehicle, current=None):
		lane = self.__getObjectByIdentifier(vehicle.lane)
		car = self.__getObjectByIdentifier("Car") 
		self.__drawObject(car, identifier, lane.location)
		obj = self.__getObjectByIdentifier(identifier)
		for p in vehicle.route:			
			self.__moveObject(obj, p)

	def destroyAllObjects_async(self,cb, current=None):
		self.work_queue.add(cb, self.destroyAllObjects, cb)
	
	def scaleObject_async(self,cb, identifier, scale, current=None):
		self.work_queue.add(cb, self.scaleObject, cb, identifier, scale)

	def setObjectVisibility_async(self,cb, identifier, visibility, current=None):
		self.work_queue.add(cb, self.setObjectVisibility, cb, identifier, visibility)

	def setObjectColor_async(self, cb, identifier, color, current=None):
		self.work_queue.add(cb, self.setObjectColor, cb, identifier, color)

	def destroyObject_async(self, cb, identifier, current=None):
		self.work_queue.add(cb, self.destroyObject, cb, identifier)

	def setObjectLocation_async(self, cb, identifier, location, current=None):
		self.work_queue.add(cb, self.setObjectLocation, cb, identifier, location)

	def setObjectRotation_async(self, cb, identifier, axis, angle, current=None):
		self.work_queue.add(cb, self.setObjectRotation, cb, identifier, axis, angle)

	def moveObject_async(self, cb, identifier, location, current=None):
		self.work_queue.add(cb, self.moveObject, cb, identifier, location)
	
	#MIRAR SI SE PUEDE HACER Y SI NO QUITARLO
	def joinObjects_async(self,cb, identifiers, current=None):
		self.work_queue.add(cb, self.joinObjects, cb, identifiers)

	def paintDetectedEventByCoords_async(self,cb, identifier, prototype, location, current=None):
		self.work_queue.add(cb, self.paintDetectedEventByCoords, cb, identifier, prototype, location)

	def paintDetectedEventByLocationName_async(self,cb, identifier, prototype, locationName, current=None):
		self.work_queue.add(cb, self.paintDetectedEventByLocationName, cb, identifier, prototype, locationName)

	def paintVehicleByCoords_async(self, cb, identifier, prototype, location, current=None):
		self.work_queue.add(cb, self.paintVehicleByCoords, cb, identifier, prototype, location)

	def paintVehicleByLocationName_async(self, cb, identifier, prototype, locationName, current=None):
		self.work_queue.add(cb, self.paintVehicleByLocationName, cb, identifier, prototype, locationName)

	def paintPersonByCoords_async(self, cb, identifier, prototype, location, current=None):
		self.work_queue.add(cb, self.paintPersonByCoords, cb, identifier, prototype, location)

	def paintPersonByLocationName_async(self, cb, identifier, prototype, locationName, current=None):
		self.work_queue.add(cb, self.paintPersonByLocationName, cb, identifier, prototype, locationName)

	def paintRouteByCoords_async(self, cb, identifier, route, color, current=None):
		self.work_queue.add(cb, self.paintRouteByCoords, cb, identifier, route, color)

	def paintRouteByNames_async(self, cb, identifier, route, color, current=None):
		self.work_queue.add(cb, self.paintRouteByNames, cb, identifier, route, color)

	def paintServiceByCoords_async(self, cb, identifier, prototype, location, current=None):
		self.work_queue.add(cb, self.paintServiceByCoords, cb, identifier, prototype, location)

	def paintServiceByLocationName_async(self, cb, identifier, prototype, locationName, current=None):
		self.work_queue.add(cb, self.paintServiceByLocationName, cb, identifier, prototype, locationName)

	def paintCircularAreaByCoords_async(self, cb, identifier, color, location, radius, current=None):
		self.work_queue.add(cb, self.paintCircularAreaByCoords, cb, identifier, color, location, radius)

	def paintCircularAreaByLocationName_async(self, cb, identifier, color, locationName, radius, current=None):
		self.work_queue.add(cb, self.paintCircularAreaByLocationName, cb, identifier, color, locationName, radius)

	def paintDesiredAreaByCoords_async(self, cb, identifier, color, location, vertices, current=None):
		self.work_queue.add(cb, self.paintDesiredAreaByCoords, cb, identifier, color, location, vertices)

	def paintDesiredAreaByLocationName_async(self, cb, identifier, color, locationName, vertices, current=None):
		self.work_queue.add(cb, self.paintDesiredAreaByLocationName, cb, identifier, color, locationName, vertices)
#IMPLEMENTAR
	def paintSensorByCoords_async(self, cb, identifier, prototype, location, current=None):
		pass

	def paintSensorByLocationName_async(self, cb, identifier, prototype, locationName, current=None):
		pass

	def accessSensor_async(self, cb, identifier, current=None):
		self.work_queue.add(cb, self.accessSensor, cb, identifier)

	def setActiveCamera_async(self, cb, identifier, current=None):
		self.work_queue.add(cb, self.setActiveCamera, cb, identifier)

	def changeObjectIdentifier_async(self, cb, oldIdentifier, newIdentifier, current=None):
		self.work_queue.add(cb, self.changeObjectIdentifier, cb, oldIdentifier, newIdentifier)

	def eventGenerator_async(self, cb, identifier, vehicle, current=None):
		self.work_queue.add(cb, self.eventGenerator, cb, identifier, vehicle)

class Server(Ice.Application):
	def __init__(self):
		args = [sys.argv[0], '--Ice.Config={}'.format('geva.config')]
		self.broker = Ice.initialize(args)
		work_queue = WorkQueue()
		self.servant = visualizerI(work_queue)
		adapter = self.broker.createObjectAdapter("VisualizerAdapter")
		print (adapter.add(self.servant, self.broker.stringToIdentity("visualizer1")))
		adapter.activate()
