#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017

from requests import get
from addon_utils import check, paths, enable

class ManagerOSM:
	coordinates = "-3.967438,38.961811,-3.876801,39.002377"
	url = "http://overpass.osm.rambler.ru/cgi/xapi_meta?*[bbox="+coordinates+"]"
	filename = "osmData.osm"
	addon = 'io_import_scene_osm'

	def downloadOSMFile(self):
		print("Downloading OSM file, it can take a while")
		with open(self.filename, "wb") as file:
			response = get(self.url)
			file.write(response.content)

	def enableAddons(self):
		is_enabled, is_loaded = check(self.addon)
		if not is_loaded:			
			enable(self.addon)
			print("%s enabled" % self.addon)
			print("Now you have to import the osm file using the interface: File > Import > OpenStreetMap (.osm)")
