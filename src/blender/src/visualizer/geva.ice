module geva{
	struct Point3D{
		float x;
		float y;
		float z;
	};

	struct Angles{
		float x;
		float y;
		float z;
	};

	struct Scale{
		float x;
		float y;
		float z;
	};

	["python:seq:list"] sequence<Point3D> PointSeq;
	["python:seq:list"] sequence<string> Identifiers;

	struct Vehicle{
		string lane;
		PointSeq route;
	};	

	interface visualizer{
		["amd"] void paintDetectedEventByCoords(string identifier, string prototype, Point3D location);
		["amd"] void paintDetectedEventByLocationName(string identifier, string prototype, string locationName);
		["amd"] void paintVehicleByCoords(string identifier, string prototype, Point3D location);
		["amd"] void paintVehicleByLocationName(string identifier, string prototype, string locationName);
		["amd"] void paintPersonByCoords(string identifier, string prototype, Point3D location);
		["amd"] void paintPersonByLocationName(string identifier, string prototype, string locationName);
		["amd"] void paintRouteByCoords(string identifier, PointSeq route, string color);
		["amd"] void paintRouteByNames(string identifier, Identifiers route, string color);
		["amd"] void paintServiceByCoords(string identifier, string prototype, Point3D location);
		["amd"] void paintServiceByLocationName(string identifier, string prototype, string locationName);
		["amd"] void paintCircularAreaByCoords(string identifier, string color, Point3D location, int radius);
		["amd"] void paintCircularAreaByLocationName(string identifier, string color, string locationName, int radius);
		["amd"] void paintDesiredAreaByCoords(string identifier, string color, Point3D location, PointSeq vertices);
		["amd"] void paintDesiredAreaByLocationName(string identifier, string color, string locationName, PointSeq vertices);
		["amd"] void paintSensorByCoords(string identifier, string prototype, Point3D location, PointSeq coverage);
		["amd"] void paintSensorByLocationName(string identifier, string prototype, string locationName, PointSeq coverage);
		["amd"] void eventGenerator(string identifier, Vehicle v);
		["amd"] void setObjectLocation(string identifier, Point3D location);
		["amd"] void setObjectRotation(string identifier, Angles ang);
		["amd"] void joinObjects(Identifiers ids);
		["amd"] void destroyObject(string identifier);
		["amd"] void setObjectVisibility(string identifier, bool visibility);
		["amd"] void setActiveCamera(string identifier);
		["amd"] void changeObjectIdentifier(string oldIdentifier, string newIdentifier);
		["amd"] void scaleObject(string identifier, Scale sc);
		["amd"] void accessSensor(string identifier);
		["amd"] void destroyAllObjects();
		["amd"] void setObjectColor(string identifier, string color);
		["amd"] void moveObject(string identifier, Point3D location);
	};
};
