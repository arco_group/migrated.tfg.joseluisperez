#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# Jose Luis Perez Ramirez
# Escuela Superior de Informatica - Universidad de Castilla-La Mancha
# Trabajo Fin de Grado: "Generador de Eventos para un entorno de Virtualidad Aumentada"
# Curso 2016/2017
"usage: {} <proxy to server>"

import sys
import Ice
Ice.loadSlice('geva.ice')
import geva
import json
import time

class Client(Ice.Application):

	def __init__(self):
		self.center = geva.Point3D(0.0,0.0,0.0);
		self.visualizer = None

	def setObjectLocationExample(self):
		self.visualizer.setObjectLocation("Cube",self.center)
		print("Location has been changed")

	def setObjectRotationExample(self):
		self.visualizer.setObjectRotation("Cube", geva.Angles(15.0,0.0,0.0))
		print("Rotation has been changed")

	def joinObjectsExample(self):
		self.visualizer.joinObjects(["Cylinder", "Sphere"])
		print("Objects have been joined")

	def paintDetectedEventByCoordsExample(self):
		self.visualizer.paintDetectedEventByCoords("Fire","Sphere",self.center)
		print("the event has been painted")

	def paintDetectedEventByLocationNameExample(self):
		self.visualizer.paintDetectedEventByLocationName("Fire","Cube","Cylinder")
		print("the event has been painted")

	def paintVehicleByCoordsExample(self):
		self.visualizer.paintVehicleByCoords("Car","CarModel",geva.Point3D(20.4363,-85.7899,1.22101))
		print("Car has been painted")

	def paintVehicleByLocationNameExample(self):
		self.visualizer.paintVehicleByLocationName("Car","CarModel","Spawner")
		print("Car has been painted")

	def paintPersonByCoordsExample(self):
		self.visualizer.paintPersonByCoords("Person","Cube",self.center)
		print("Person has been painted")

	def paintPersonByLocationNameExample(self):
		self.visualizer.paintPersonByLocationName("Person","Cube","Cylinder")
		print("Person has been painted")

	def paintRouteByCoordsExample(self):
		route = []
		route.append(geva.Point3D(-28.76714,18.73418,1.22085))
		route.append(geva.Point3D(-47.92181,2.00774,1.22095))
		route.append(geva.Point3D(-56.69845,-7.47027,1.22106))
		route.append(geva.Point3D(-82.16212,-33.48228,1.22100))
		route.append(geva.Point3D(-77.29664,-45.35088,1.22085))
		route.append(geva.Point3D(-108.60762,-86.51528,1.22128))
		route.append(geva.Point3D(-74.29897,-172.81764,1.22113))
		self.visualizer.paintRouteByCoords("Route", route, "green")
		print("Route has been painted")

	def paintRouteByNamesExample(self):
		route = []
		route.append("Cube")
		route.append("Sphere")
		route.append("Cylinder")
		self.visualizer.paintRouteByNames("Route", route, "green")
		print("Route has been painted")

	def paintServiceByCoordsExample(self):
		self.visualizer.paintServiceByCoords("Wifi","Cube",self.center)
		print("service has been painted")

	def paintServiceByLocationNameExample(self):
		self.visualizer.paintServiceByLocationName("Wifi","Cube","Cylinder")
		print("service has been painted")

	def paintCircularAreaByCoordsExample(self):
		self.visualizer.paintCircularAreaByCoords("propagation","red",self.center,20)
		print("Circular area has been painted")

	def paintCircularAreaByLocationNameExample(self):
		self.visualizer.paintCircularAreaByLocationName("propagation","red","Cylinder",4)
		print("Circular area has been painted")

	def paintDesiredAreaByCoordsExample(self):
		self.visualizer.paintDesiredAreaByCoords("propagation","green",self.center, [geva.Point3D(0,0,0),geva.Point3D(1,0,0),geva.Point3D(2,1,0),geva.Point3D(2,2,0)])
		print("Desired area has been painted")

	def paintDesiredAreaByLocationNameExample(self):
		self.visualizer.paintDesiredAreaByLocationName("propagation","red","Cylinder", [geva.Point3D(0,0,0),geva.Point3D(1,0,0),geva.Point3D(2,1,0),geva.Point3D(2,2,0)])
		print("Desired area has been painted")

	def destroyObjectExample(self):
		self.visualizer.destroyObject("Car")
		print("Object has been destroyed")

	def setObjectVisibilityExample(self):
		self.visualizer.setObjectVisibility("Sphere",True)
		print("Visibility has been changed")

	def setActiveCameraExample(self):
		self.visualizer.setActiveCamera("Camera")
		print("Specified camera has been selected")

	def changeObjectIdentifierExample(self):
		self.visualizer.changeObjectIdentifier("Sphere","newSphere")
		print("Identifier has been updated")

	def scaleObjectExample(self):
		self.visualizer.scaleObject("Sphere",geva.Scale(2.0,2.0,2.0))
		print("Scale has been updated")

	def destroyAllObjectsExample(self):
		self.visualizer.destroyAllObjects()
		print("All objects have been destroyed")
	
	def setObjectColorExample(self):
		self.visualizer.setObjectColor("Cube","blue")
		print("Color has been changed")

	def eventGeneratorExample(self):		
		route = []
		for i in xrange(2,0,-1):
			for j in xrange(9,0,-1):
				route.append(geva.Point3D(j*i*i,i*6,0))
			self.visualizer.eventGenerator("Car"+str(i), geva.Vehicle("Lane"+str(i),route))
			del route[:]

	def cameraEventGenerator(self):
		data = []
		
		with open('roadData.txt') as f1:
			for line in f1:
				roadData = line.split(",")

			listOfIndexes = [0] * int(roadData[1])
			listOfCubes = [[None] for i in range(int(roadData[0]))] 

		with open('data.json') as f: 
			data = json.load(f)

		while len(data['objects']) > 0:
			frame = data['objects'].pop(0)

			while (len(data['objects']) > 0) and ((data['objects'][0])['class'] == 'Vehicle'):
				v = data['objects'].pop(0)
				index = listOfIndexes[v['lane']]
			if (((listOfCubes[v['lane']])[index]) is None):          
				self.paintVehicleByLocationNameExample()
				((listOfCubes[v['lane']])[index]) = True
				self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
				lastXCoord = v['xCoord']
				listOfIndexes[v['lane']] += 1
				if listOfIndexes[v['lane']] >= int(roadData[0]):
					listOfIndexes[v['lane']] = 0
			else:
				if(v['xCoord'] > lastXCoord):
					self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
					lastXCoord = v['xCoord']
					listOfIndexes[v['lane']] += 1
					if listOfIndexes[v['lane']] >= int(roadData[0]):
						listOfIndexes[v['lane']] = 0
				else:
					self.destroyObjectExample()
					if int(roadData[0]) == 1:
						self.paintVehicleByLocationNameExample()
					self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
					lastXCoord = v['xCoord']

	def run(self, argv):
		base = self.communicator().stringToProxy(argv[1])
		print (argv[1])
		
		self.visualizer = geva.visualizerPrx.checkedCast(base)

		if not self.visualizer:
			raise RuntimeError("Invalid proxy")
		
		#Código del opencv

		#self.paintDetectedEventByCoordsExample()
		#self.paintDetectedEventByLocationNameExample()
		#self.paintVehicleByCoordsExample()
		#self.paintVehicleByLocationNameExample()
		#self.paintPersonByCoordsExample()
		#self.paintPersonByLocationNameExample()
		#self.paintServiceByCoordsExample()
		#self.paintServiceByLocationNameExample()
		#self.setObjectLocationExample()
		#self.setObjectRotationExample()
		#self.paintCircularAreaByCoordsExample()
		#self.paintCircularAreaByLocationNameExample()
		#self.paintDesiredAreaByCoordsExample()
		#self.paintDesiredAreaByLocationNameExample()		
		#self.paintRouteByCoordsExample()
		#self.paintRouteByNamesExample()
		#self.destroyObjectExample()
		#self.setObjectVisibilityExample()
		#self.changeObjectIdentifierExample()
		#self.scaleObjectExample()
		#self.destroyAllObjectsExample()
		#self.setObjectColorExample()

		#self.eventGeneratorExample()
		self.cameraEventGenerator()

if len(sys.argv) != 2:
	print(__doc__.format(__file__))
	sys.exit(1)

app = Client()
sys.exit(app.main(sys.argv))
