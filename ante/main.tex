\documentclass{pre-tfg}

%\showhelp

\title{GEVA: Generador de Eventos para un entorno de Virtualidad Aumentada.}
\author{José Luis Pérez Ramírez}
\advisorFirst{Félix Jesús Villanueva Molina}
\advisorDepartment{TECNOLOGÍAS Y SISTEMAS DE INFORMACIÓN}
\intensification{INGENIERÍA DE COMPUTADORES}
\docdate{2017}{Enero}

\usepackage{tikz}
\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}


\begin{document}

\maketitle
\tableofcontents

\newpage

\section{INTRODUCCIÓN}
No es un secreto que hoy en día hay un gran interés suscitado por las ciudades inteligentes debido a los beneficios que pueden suponer.

¿Pero qué es una ciudad inteligente? Como definición general, podemos decir que una ciudad inteligente es cualquier ciudad que hace uso de las tecnologías de la información y de la comunicación (TIC) con el fin de proveerla de una infraestructura que garantice un aumento de la calidad de vida de sus ciudadanos, un desarrollo sostenible, una participación ciudadana activa y una mayor eficacia de los recursos disponibles \cite{endesa}.  Pero aunque esa es la definición general, en la realidad las ciudades inteligentes se centran en utilizar las TIC para conseguir un objetivo concreto. Para Rudolf Giffinger, por ejemplo, dichos objetivos pueden clasificarse basándose en los siguientes criterios \cite{objetivos}:

\begin{itemize}
	\item Economía
	\item Movilidad
	\item Medioambiente
	\item Habitantes
	\item Forma de vida
	\item Administración
\end{itemize}

Centrándonos en este último, una posible utilidad sería representar
una ciudad en un mundo virtual en la que sus ciudadanos, vehículos y
construcciones serían objetos contenidos en dicho mundo, este mundo
nos serviría para mostrar todo tipo de información y entender, de una
forma mas intuitiva, eventos, tendencias, situaciones de crisis,
etc.. Un ejemplo de mundo real representado de forma virtual puede verse en la figura 1,
que muestra el puerto real de Valencia y su correspondencia
virtual. Este es el objetivo del proyecto CitiSim del grupo de
investigación ARCO de la UCLM.

\begin{figure}[!h]
\centering
   \includegraphics[width=14cm]{figura1.png}
\caption{Puerto real de Valencia y su correspondencia virtual}
\end{figure}

Este trabajo fin de grado consistirá en desarrollar un sistema que capture mediante una cámara el tráfico de una calle de Ciudad Real para procesar el vídeo resultante mediante la librería OpenCV \cite{opencv} para aplicarle una serie de filtros con el fin de obtener velocidad, dirección y sentido tanto de los peatones como de los vehículos para, finalmente, representarlos en un mundo virtual haciendo uso del Game Engine de Blender \cite{blender}. Cabe indicar que en dicho mundo virtual habrá una cámara que simule la cámara real por lo que los objetos virtuales se verán de la misma forma que los objetos reales captados por la cámara real.

Puede consultarse el esquema del sistema a desarrollar durante el proyecto en la figura 2. En dicha figura puede observarse que el mundo virtual pertenece a Citisim, un simulador para la generación y representación de eventos en una ciudad inteligente que fue desarrollado por Ana Rubio Ruiz en su TFG \cite{citisim}. Se ha querido representar con esto último que el sistema a desarrollar durante el proyecto podría ser integrado con el simulador Citisim para analizar diferentes eventos en la representación virtual de una ciudad inteligente.

\begin{figure}[!h]
\centering
   \includegraphics[width=16cm]{figura2.png}
\caption{Esquema del sistema a desarrollar}
\end{figure}

A lo largo del presente documento justificaré la inclusión de este Trabajo Fin de Grado dentro de la intensificación escogida e indicaré cuáles son los objetivos planteados, el método y las fases de trabajo así como los medios que se pretenden utilizar a lo largo de su desarrollo.


\section{TECNOLOGÍA ESPECÍFICA / INTENSIFICACIÓN / ITINERARIO CURSADO POR EL ALUMNO}
En el cuadro 1 puede verse la Tecnología Específica cursada, y en el cuadro 2 pueden verse las competencias específicas abordadas en este TFG.
\begin{table}[hp]
  \centering
  \caption{Tecnología Específica cursada por el alumno}
  \label{tab:tec-especifica}

  \zebrarows{1}
  \begin{tabular}{p{0.6\textwidth}}
    \textbf{Marcar la tecnología cursada} \\
    \hline
    \quad \enspace Tecnologías de la Información \\
    \quad \enspace Computación \\
    \quad \enspace Ingeniería del Software \\
    \checkmark \enspace	Ingeniería de Computadores \\
    \hline
  \end{tabular}
\end{table}

\begin{table}[hp]
  \centering
  \caption{Justificación de las competencias específicas abordadas en el TFG}
  \label{tab:competencias}

  \zebrarows{1}
  \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
    \textbf{Competencia} & \textbf{Justificación} \\
    \hline
    Capacidad de diseñar e implementar software de sistema y de comunicaciones.  & El sistema a desarrollar en el proyecto obtendrá la información de cámaras desplegadas en un mundo real que deberá ser transmitida a través de algún mecanismo de comunicación puesto que dicho sistema va a ser distribuido.\\
Capacidad de analizar, evaluar y seleccionar las plataformas hardware
    y software más adecuadas para el soporte de aplicaciones
    empotradas y de tiempo real. & Se evaluarán diferentes tipos de
                                   cámaras para su integración en el
                                   sistema y para la obtención de
                                   imágenes. Implementación del
                                   sistema usando cámaras empotradas
                                   en una maqueta. \\
Capacidad para analizar, evaluar, seleccionar y configurar plataformas hardware para el desarrollo y ejecución de aplicaciones y servicios informáticos. & El sistema deberá ser configurado de acuerdo al objetivo de este proyecto con los recursos, paquetes software y entornos de desarrollo más adecuados para conseguir dicho objetivo.\\
    \hline
  \end{tabular}
\end{table}

\pagebreak
\section{OBJETIVOS}

El objetivo general del proyecto será la detección de objetos del mundo real mediante una cámara para luego representarlos en un mundo virtual. Para lograr este objetivo, será necesario conseguir también los siguientes subobjetivos:

\begin{enumerate}
	\item \textbf{Evaluar algoritmos de identificación de objetos en vídeos de tiempo real.}  Habrá que determinar qué algoritmos son los más adecuados para aplicar una serie de filtros a los objetos reales detectados para seleccionar aquellos que nos interesen en el desarrollo del proyecto (vehículos y personas)  para luego poder representarlos en tiempo real en un mundo virtual.
	\item \textbf{Representación en mundos virtuales de objetos
            identificados en el paso anterior.} A partir de los
          objetos identificados en el punto anterior, se estudiará y
          desarrollará el software para la representación de objetos
          reales en el mundo virtual trabajando, escalas, modelos de
          representación, etc..
	\item \textbf{Evaluar los problemas de prestaciones del mundo virtual e identificar las limitaciones.} Se deberán señalar los principales problemas que aparezcan durante el desarrollo del proyecto así  como las limitaciones detectadas a la hora de representar un objeto real en el mundo virtual así como las posibles soluciones.
\end{enumerate}


\section{MÉTODO Y FASES DE TRABAJO}

Para el desarrollo de este proyecto se ha decidido seguir una metodología ágil, ya que este tipo de metodologías se adaptan mejor a los cambios que se vayan sucediendo a lo largo del desarrollo y también a los posibles problemas durante el mismo, permitiendo solucionarlos de forma rápida cuando se requiera. Los cambios anteriores pueden deberse a necesidades que no se hayan tenido en cuenta al inicio del desarrollo o bien debidos a mejorar una característica del proyecto que ya se había contemplado. Sea cual sea el motivo, con una metodología ágil podrá adaptarse el proyecto a esos cambios.

Las fases generales de las que va a constar el trabajo son las siguientes:

\begin{enumerate}
	\item \textbf{Elección de los medios a utilizar.} Se deberá investigar acerca de los mejores medios hardware y software para la realización del proyecto.
	\item \textbf{Evaluación de los algoritmos de representación.} Se tendrán que evaluar los posibles algoritmos y obtener el más adecuado para obtener las personas y vehículos de un vídeo en tiempo real.
	\item \textbf{Estudio de las formas de representación.} Se tendrá que estudiar cuál la mejor forma de representación de los vehículos y personas en un mundo virtual una vez obtenidos.
	\item \textbf{Evaluación de las limitaciones en la representación.} Se deberán evaluar cuáles son las limitaciones surgidas a la hora de representar los objetos en el mundo virtual y actuar en consecuencia para solucionarlas y/o paliarlas en la medida de lo posible.
	\item \textbf{Integración final.} Integración de los elementos obtenidos anteriormente para conseguir como resultado el sistema final.
\end{enumerate}

\section{MEDIOS QUE SE PRETENDEN UTILIZAR}
A continuación se describen los medios hardware y software necesarios para la realización del proyecto:

\subsection{Medios Hardware}

\begin{itemize}
	\item \textbf{Fuente de información.} La información de las personas y vehículos que se representarán en el mundo virtual será obtenida de un vídeo generado por una cámara que estará grabando una zona con dichos objetos reales.
	\item \textbf{Equipo de trabajo.} Para el desarrollo de este proyecto, el equipo a utilizar va a tener las especificaciones indicadas en el cuadro 3:
\end{itemize}

\begin{table}[hp]
  \centering
  \caption{Equipo elegido para el desarrollo del proyecto.}
  \label{tab:medios-hardware}

  \zebrarows{1}
  \begin{tabular}{p{0.6\textwidth}}
    \textbf{Aspire V3-571G} \\
    \hline
    Intel@ Core I7-3632QM 2,2 GHz with Turbo Boost up to 3,2 GHz\\
    NVIDIA@ GeForce@ 710M with 2 GB Dedicated VRAM\\
    Pantalla 15,6 HD LED LCD\\
    DVD-Super Multi DL drive\\
    4 GB DDR3 Memory\\
    Acer Nplify 802,11a/g/n + BT 4.0\\
    750 GB HDD\\
    6-cell Li-ion battery\\
    \hline
  \end{tabular}
\end{table}


\subsection{Medios Software}

\begin{itemize}
	\item \textbf{Aplicación del algoritmo para aplicar los filtros al vídeo.} Para aplicar los filtros que permitan recopilar la información necesaria sobre las personas y vehículos se ha determinado que la mejor elección es OpenCV \cite{opencv}, por ser de código abierto y por la forma en la que permite aplicar filtros, más sencilla que con cualquier otro software.
	\item \textbf{Representación de los objetos reales en el mundo virtual.} Tras haber evaluado las distintas opciones para tal fin, se ha llegado a la conclusión que la mejor opción es Blender (más concretamente su Game Engine) \cite{blender} por ser de código abierto y utilizar como lenguaje de programación Python \cite{python}, que es más sencillo que los utilizados por otros software similares a Blender.
	\item \textbf{Redacción de la memoria del proyecto.} Se ha decido utilizar el procesador de texto \LaTeX{} \cite{latex} para redactar de manera sencilla la memoria del proyecto gracias a sus características adecuadas para el desarrollo de este tipo de documentos, además de ser de código abierto.
	\item \textbf{Repositorio.}  Para el almacenamiento web de todos los archivos de la memoria así como el código fuente del proyecto se ha decidido utilizar BitBucket \cite{bitbucket}, por proporcionar la posibilidad de tener repositorios privados con un máximo de 5 miembros sin necesidad de pago. También se consideró la posibilidad de utilizar GitHub, pero al ser de pago los repositorios privados, se descartó está opción.
\end{itemize}


\pagebreak
\section{REFERENCIAS}
\bibliographystyle{plain}

\begin{thebibliography}{4}


\bibitem{endesa} \texttt{\href{http://www.arcus-global.com/wp/que-es-una-smart-city/}{arcus-global.com}}. Concepto de ciudad inteligente.

\bibitem{objetivos} Rudolf Giffinger, \texttt{\href{http://www.smart-cities.eu/download/smart_cities_final_report.pdf}{Smart cities – Ranking of European medium-sized cities}}, Centre of Regional Science (2007)
\bibitem{citisim} \texttt{\href{https://es.wikipedia.org/wiki/Ciudad\_inteligente\#Definici.C3.B3n}{Citisim}}. CITISIM: Simulador para generación y representación de eventos en la Smart City, desarrollado por Ana Rubio Ruiz.

\bibitem{opencv} \texttt{\href{http://docs.opencv.org/3.1.0/dc/d2e/tutorial_py_image_display.html}{OpenCV tutorial}}. Getting started with OpenCV.

\bibitem{python} \texttt{\href{https://docs.python.org/3/tutorial/}{Python tutorials}}, Introduction with Python.

\bibitem{blender} Blender, tutorial for beginners \texttt{\href{https://cgcookie.com/flow/introduction-to-blender/}{Introduction to Blender}}

\bibitem{bitbucket} Introducing mercurial and Bitbucket | Atlassian. \texttt{\href{https://confluence.atlassian.com/bitbucket/tutorial-learn-mercurial-with-bitbucket-cloud-759857394.html}{Bitbucket tutorials}}

\bibitem{latex} \texttt{\href{http://www.maths.tcd.ie/~dwilkins/LaTeXPrimer/GSWLaTeX.pdf}{Introduction to LaTeX}}, Getting started with LaTeX.

\end{thebibliography}

\end{document}


% Local Variables:
% coding: utf-8
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% mode: latex
% End:
