\chapter{Introducción}
\label{chap:introduccion}

No es un secreto que hoy en día hay un gran interés suscitado por las ciudades inteligentes debido a los beneficios que pueden suponer.

¿Pero qué es una ciudad inteligente? Como definición general, podemos decir que una ciudad inteligente es cualquier ciudad que hace uso de las \acf{TIC} con el fin de proveerla de una infraestructura que garantice un aumento de la calidad de vida de sus ciudadanos, un desarrollo sostenible, una participación ciudadana activa y una mayor eficacia de los recursos disponibles \cite{smart-city}. Pero aunque esa es la definición general, en la realidad las ciudades inteligentes se centran en utilizar las TIC para conseguir un objetivo concreto. Para Rudolf Giffinger, por ejemplo, dichos objetivos pueden clasificarse basándose en los siguientes criterios \cite{objetivos}:

\begin{itemize}
	\item Economía
	\item Movilidad
	\item Medio ambiente
	\item Habitantes
	\item Forma de vida
	\item Administración
\end{itemize}

Centrándonos en este último, una posible utilidad sería representar una ciudad en un mundo virtual en la que sus ciudadanos, vehículos y construcciones serían objetos contenidos en dicho mundo, el cual
nos serviría para mostrar todo tipo de información y entender, de una forma mas intuitiva, eventos, tendencias, situaciones de crisis, etc. Un ejemplo de mundo real representado de forma virtual puede verse en la figura \ref{puerto-valencia}, que muestra el puerto real de Valencia y su correspondencia virtual. Este es el objetivo del proyecto Citisim del grupo de investigación \acf{ARCO} de la \acf{UCLM}.

\begin{figure}[!h]
\centering
   \includegraphics[width=14cm]{figs/puerto-valencia.png}
\caption{Puerto real de Valencia y su correspondencia virtual}
\label{puerto-valencia}
\end{figure}

El proyecto GEVA viene motivado por las necesidades de representación de información en un mundo virtual que el proyecto Citisim utilizará para la interpretación de la información real en un mundo virtual, lo que se conoce como virtualidad aumentada. Esta forma de representación de la información presenta una forma novedosa y fácil de interpretar situaciones dentro de la \textit{smart city}. El proyecto Citisim se encuentra en su primer año de desarrollo, es un proyecto con el sello europeo ITEA 3 y financiado por las correspondientes agencias locales en España, Rumanía y Turquía.

Este trabajo fin de grado consistirá en desarrollar un sistema que soporte diferentes generadores de eventos que serán representados en un mundo virtual previamente creado mediante el \acf{BGE}\footnote{https://www.blender.org/}, ya que el proyecto Citisim hacía uso de esa herramienta, por lo que su uso ya venía impuesto para el presente \acf{TFG}. Para la representación de los eventos generados (ya sea mediante la plataforma o mediante un acontecimiento externo como por ejemplo la activación de una alarma), se desarrollará una \acs{API} que permitirá a los usuarios representar peatones, vehículos, áreas, eventos (por ejemplo un incendio), etc. 

Puede consultarse el esquema del sistema a desarrollar durante el proyecto en la figura \ref{esquema}. En dicha figura puede observarse que el mundo virtual pertenece a Citisim\cite{citisim}, un simulador para la generación y representación de eventos en una ciudad inteligente que fue desarrollado por Ana Rubio Ruiz en su \acs{TFG}\cite{tfg-citisim}. Dicho proyecto crea un mundo 3D virtual mediante la herramienta \textit{Blender} que representa fielmente a una ciudad real como, por ejemplo, la de la figura \ref{puerto-valencia}.

Por tanto, este proyecto podrá ser integrado con el de Citisim para representar en el mundo virtual información de diversa índole, sensores, cámaras, información estadística, etc. Como se justificará más adelante, se hará uso del \textit{middleware} \textit{ZeroC Ice}\footnote{https://zeroc.com/products/ice} para poner a disposición de las distintas fuentes de información una \acs{API} que puedan invocar de forma remota para representar la información que capten en el mundo virtual. A modo de ejemplo, se desarrollará un generador de eventos capaz de analizar cámaras en tiempo real mediante la librería OpenCV\footnote{http://opencv.org} de forma que introduzcan eventos en los que aparezcan vehículos y peatones y representen dichos eventos mediante la \acs{API} antes comentada. Por comodidad, se hará uso de un vídeo previamente grabado, pero la idea sería que cada cámara generara eventos en tiempo real. Este generador es de los más complejos y exigentes que Citisim tendrá, ya que requiere, como veremos, estudiar algoritmos de visión por computador, conversión de coordenadas, variación del tiempo, etc. Cabe indicar que en el mundo virtual habrá una cámara que simule la cámara real por lo que los objetos virtuales se verán de la misma forma que los objetos reales captados por la cámara real.

\begin{figure}[!h]
\centering
   \includegraphics[width=17cm]{figs/esquema.png}
\caption{Esquema del sistema a desarrollar}
\label{esquema}
\end{figure}

\section{Estructura del documento}

A continuación se listan todos los capítulos que componen esta memoria junto con una breve descripción de lo que puede encontrarse en cada uno de ellos:

\begin{definitionlist}
\item[Capítulo \ref{chap:objetivos}: \nameref{chap:objetivos}]
Se listan las distintas metas que se desean alcanzar con el desarrollo de este proyecto así como los requisitos que se deben cumplir para lograrlo.
\item[Capítulo \ref{chap:antecedentes}: \nameref{chap:antecedentes}] 
Se aporta de una forma completa información acerca los conceptos tratados a lo largo del presente documento. Tales conceptos son: computación gráfica y motores gráficos, visión por computador y \textit{OpenCV}, algoritmos de detección de movimiento (el basado en \textit{cascades} y el de eliminación del fondo), sistemas distribuidos, formatos de datos y virtualización.
 \item[Capítulo \ref{chap:metodologia}: \nameref{chap:metodologia}]
Se describe la metodología de trabajo que se ha seguido durante el desarrollo del proyecto, los requisitos hardware y software necesarios y las iteraciones llevadas a cabo durante el desarrollo.
\item[Capítulo \ref{chap:arquitectura}: \nameref{chap:arquitectura}]
Se detallan las distintas partes que componen el sistema desarrollado junto con su función así como el flujo de datos entre ellas.
\item[Capítulo \ref{chap:resultados}: \nameref{chap:resultados}]
Se ofrece un resumen de la plataforma tras el desarrollo del proyecto y unos ejemplos que ilustran el funcionamiento de dicha plataforma. 
\item[Capítulo \ref{chap:conclusiones}: \nameref{chap:conclusiones}]
Se analizará el grado de consecución de los objetivos planteados una vez realizado el proyecto, se describirán las dificultades encontradas durante el desarrollo del mismo y se realizarán diferentes propuestas de trabajo con el fin de mejorar el sistema.
\end{definitionlist}


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
