\chapter{Metodología de trabajo}
\label{chap:metodologia}

\noindent
\drop{E}{n} este capítulo se describirá la metodología de trabajo que se ha seguido y el por qué de su elección, se indicarán las distintas iteraciones realizadas durante el desarrollo del proyecto y se detallarán las herramientas utilizadas.

\section{Elección de la metodología}
Una de las primeras decisiones que deben ser afrontadas es la elección de una metodología de trabajo, ya que ésta definirá el camino a seguir durante la realización del proyecto. En mi caso, y tras analizar las ventajas de las metodologías ágiles frente a las tradicionales, he decidido seleccionar una metodología ágil. 

\subsection{Metodologías ágiles}
Anteriormente afirmaba que había escogido una metodología ágil pero, ¿por qué?. En primer lugar, este tipo de metodologías permite clasificar las etapas de un proyecto en pequeños bloques — también llamados iteraciones —, que a su vez contienen las tareas propias de cada etapa. Además, posibilita la clasificación de las tareas según su prioridad, dando lugar a la optimización de los recursos disponibles. Las principales ventajas de las metodologías ágiles frente a las otras clases de metodologías son las siguientes \cite{metodologia}:

\begin{itemize}
\item Permite la posibilidad de responder rápidamente a los cambios ya que, al dividir un proyecto en pequeños bloques, no es necesario esperar a la finalización de dicho proyecto para solucionar los fallos o cambios requeridos por el cliente\footnote{en este proyecto es el tutor del \acs{TFG}}.

\item Como se ha mencionado en el punto anterior, al cliente se le otorga la posibilidad de intervenir durante la realización del proyecto en cada una de las etapas, aportando nuevas ideas modificando las ya existentes. 

\item Debido a la existencia de distintas etapas, al final de cada una de ellas se obtiene una entrega parcial del producto, permitiendo corregir los errores que se encuentren, optimizar los recursos y realizar un seguimiento del proyecto.

\item Por último, la fragmentación en etapas permite a los desarrolladores\footnote{en este proyecto únicamente hay un desarrollador: el estudiante} focalizar el esfuerzo en aquellas que consideran más prioritarias.
\end{itemize}

Por todo lo anterior se ha optado por una metodología ágil, y tras estudiar las distintas opciones, finalmente me decanto por \acf{XP}\footnote{http://www.extremeprogramming.org/}.


\subsubsection{Metodología \acs{XP}}
He escogido esta metodología porque tomándola como base y modificándola, se adapta mejor a la forma de trabajo deseada para este proyecto. 

Tiene como principal característica que se basa en la creencia de que es inevitable el cambio de requisitos durante el desarrollo de cualquier proyecto y que, por lo tanto, es mejor adaptarse a cualquier cambio durante dicho desarrollo antes que definirlos todos de antemano y luego tener que controlar los cambios.

En cuanto a las entregas, tiene la ventaja de que propone que éstas sean iterativas e incrementales, de tal forma que son más frecuentes y pueden realizarse las pruebas necesarias para comprobar el correcto funcionamiento de aquello que ya ha sido implementado y corregir los errores detectados. Ésto a su vez, permite una mayor interacción entre el estudiante y el tutor, permitiendo así una mejor adaptación a los posibles cambios en los requisitos. 

A continuación se detallan todos los principios de \acs{XP}, explicando además si pueden seguir durante el desarrollo de este proyecto y en caso afirmativo, cómo\cite{extreme1}:

\begin{itemize}
\item \textbf{Equipo completo}: todas las personas relacionadas con el proyecto forman parte de él. En el caso del proyecto GEVA, los únicos integrantes son el tutor y el estudiante.

\item \textbf{Planificación}: para llevar a cabo este principio, durante todo el desarrollo se identificarán los objetivos sucesivos a cumplir y las versiones parciales de la implementación final. Tras la consecución de cada uno de ellos, se realizará el pertinente seguimiento del proyecto.

\item \textbf{Test del cliente}: El cliente, con la ayuda del desarrollador, propondrá las pruebas deseadas para validar las mini-versiones.

\item \textbf{Versiones pequeñas}: de acuerdo a este principio, las versiones parciales obtenidas deberán ser lo suficientemente pequeñas para conseguir obtenerlas cada pocas semanas, pero siempre teniendo en mente que deberán permitir al cliente observar algo funcional y no únicamente implementaciones de código no ejecutable. En el caso del proyecto GEVA, se ha establecido que el tiempo máximo entre versiones sea de dos semanas.

\item \textbf{Diseño simple}: durante el desarrollo del proyecto se realizará siempre lo mínimo imprescindible y de la forma más sencilla con el objetivo de simplificar al máximo el código.

\item \textbf{Pareja de programadores}: no se puede llevar a cabo debido a que en este proyecto únicamente trabajará una persona, y este principio especifica que los integrantes del equipo de desarrollo trabajan por parejas, intercambiándolas con bastante frecuencia.

\item \textbf{Desarrollo guiado por las pruebas automáticas}: tras la entrega de cada versión, se realizarán las pruebas necesarias con el fin de detectar posibles errores y solucionarlos antes de proseguir con la implementación de la siguiente versión programada.

\item \textbf{Integración continua}: será obligatorio tener siempre un ejecutable funcional del proyecto y cuando se tenga una nueva funcionalidad implementada, deberá añadirse a dicho ejecutable para recompilarlo y probarlo. Con ésto conseguiremos detectar los posibles errores más fácilmente, ya que si se dejara una versión durante mucho tiempo sin actualizar mientras se implementan nuevas características del proyecto, cuando se añadiesen a la versión previa y se realizara la recompilación, si ocurriese algún error, sería muy difícil determinar la causa.

\item \textbf{El código es de todos}: cualquier integrante del equipo de desarrollo deberá tener acceso al código y conocerá su finalidad. Debido a este principio se tiene también el denominado "Desarrollo guiado por las pruebas automáticas". En el caso de GEVA, el estudiante es el único desarrollador y tiene por tanto acceso total al código.

\item \textbf{Normas de codificación}: al haber un único desarrollador, es sencillo establecer un único estilo de codificación, dando lugar a que el código del proyecto sea homogéneo.

\item \textbf{Metáforas}: este principio establece la necesidad de definir un conjunto de frases o nombres clave para asignarlas a las distintas funcionalidades del programa, con el fin de facilitar la comprensión de cada sección del código. Al igual que con el anterior principio, la existencia de un único desarrollador facilita enormemente la elección de las metáforas.

\item \textbf{Ritmo sostenible}: durante el desarrollo, el ritmo de trabajo deberá poderse mantener indefinidamente, de tal manera que no haya días en los que no se trabaje en el proyecto y que tampoco haya días con exceso de horas trabajadas. Para ayudar a conseguir lo anterior, existe el principio "Planificación", ya que sabiendo de antemano el trabajo que debe realizarse, es mucho más fácil conocer el esfuerzo necesario.

\end{itemize}

Por último se detallan las etapas del ciclo de vida de \acs{XP} que determinan el flujo de trabajo durante el desarrollo del proyecto.

\subsection{Ciclo de vida de \acs{XP}}
En primer lugar, es necesario definir qué son las historias de usuario, pues sirven como base para la organización del trabajo que debe realizarse durante el desarrollo de un proyecto. Una historia es una tarjeta de papel escrita por el cliente para identificar los requisitos software del proyecto, tanto funcionales como no funcionales. La principal ventaja de su uso es que permiten, de una forma sencilla, cambiar los requisitos en cualquier momento modificando las historias ya definidas, así como poder desgranar una historia en otras más sencillas o al contrario, unir varias historias sencillas en otra más compleja\cite{extreme2}.

Para el proyecto desarrollado, cada una de ellas tendrá asociado un nombre, el rol del usuario, una descripción de lo que se quiere conseguir y las condiciones que deben darse para aceptar su correcto funcionamiento. Durante el desarrollo del proyecto, cada historia deberá ser descompuesta en tareas de programación que se realizarán durante una iteración si fuera posible.

A continuación se detallan las etapas de las que consta el ciclo de vida de \acs{XP}, explicadas con respecto al proyecto GEVA\cite{extreme2}.

\subsubsection{Etapa I: Exploración}

En  esta  fase, el tutor del \acs{TFG}, planteará las historias de usuario que son de interés para la primera entrega. Al mismo tiempo, el estudiante deberá familiarizarse con las herramientas y prácticas utilizadas durante el desarrollo del proyecto. La duración de esta etapa no será mayor a un mes, aunque puede resultar menor por la familiaridad del estudiante con algunas de las herramientas utilizadas.

\subsubsection{Etapa II: Planificación de la Entrega}

En esta fase, el tutor determinará la prioridad de cada historia de usuario y a su vez, el estudiante deberá estimar el esfuerzo requerido para cada una de ellas. Una vez se disponga de dicha información, ambos acordarán el contenido de la primera entrega además de un cronograma. La duración de esta etapa será de no más de una semana y el plazo para cada entrega no deberá sobrepasar las 2 semanas.

\subsubsection{Etapa III: Iteraciones}

Cada entrega está formada por una o varias iteraciones de no más de dos semanas cada una. En la primera iteración se seleccionarán las historias de usuario que permitan definir la arquitectura del sistema que será utilizada en el resto del desarrollo del proyecto. Cuando se finalice la última iteración, el sistema podrá entrar en producción.

Los aspectos que determinarán qué se va a realizar en cada iteración son: las historias de usuario que no se hayan abordado anteriormente, la velocidad con la que se está desarrollando el proyecto, las pruebas de aceptación no superadas en la iteración anterior y las tareas que no hayan podido realizarse en la iteración anterior. Las tareas de una misma iteración deberán traducirse a tareas de programación, que se asignarán al estudiante.

\subsubsection{Etapa IV: Producción} 

Durante esta fase se realizarán nuevas pruebas y se estudiará el rendimiento del sistema antes de que sea trasladado al entorno del cliente (en este caso el entorno del cliente sería la plataforma virtualizada). Cabe indicar que la duración de cada iteración podría disminuir de tres semanas a una. Además, podría ser necesario incluir nuevas características del sistema a las ya existentes. Por tanto, todas las nuevas ideas que surjan serán documentadas para su consiguiente implementación en etapas posteriores. 
  
\subsubsection{Etapa V: Mantenimiento} 

Cuando la primera versión del proyecto esté en producción, será necesario comenzar el desarrollo de nuevas iteraciones mientras se mantiene el sistema funcional, por lo que serán requeridas tareas de soporte para el cliente.

\subsubsection{Etapa VI: Muerte del Proyecto} 

Esta etapa dará comienzo cuando el tutor no disponga de más historias que deban ser incluidas en el sistema, en cuyo caso será necesario satisfacer sus necesidades en términos de rendimiento o confiabilidad. Además, se generará la documentación final del sistema y no se realizarán más cambios en su arquitectura.

\section{Herramientas}

\subsection{Hardware}

\begin{itemize}
	\item \textbf{Fuente de información.} La información de las personas y vehículos que se representarán en el mundo virtual será obtenida de un vídeo generado por una cámara que estará grabando una zona con dichos objetos reales. También se utilizará \acl{OSM} para la obtención de datos de una ciudad real para obtener posteriormente su correspondencia virtual.
	\item \textbf{Equipo de trabajo.} Para el desarrollo de este proyecto, el equipo a utilizar va a tener las especificaciones indicadas en el cuadro \ref{cuadro-especificaciones}.
\end{itemize}

\begin{table}[hp]
  \centering
  \caption{Equipo elegido para el desarrollo del proyecto.}
  \label{cuadro-especificaciones}

  
  \begin{tabular}{p{0.6\textwidth}}
    \textbf{Aspire V3-571G} \\
    \hline
    Intel@ Core I7-3632QM 2,2 GHz with Turbo Boost up to 3,2 GHz\\
    NVIDIA@ GeForce@ 710M with 2 GB Dedicated VRAM\\
    Pantalla 15,6 HD LED LCD\\
    DVD-Super Multi DL drive\\
    4 GB DDR3 Memory\\
    Acer Nplify 802,11a/g/n + BT 4.0\\
    750 GB HDD\\
    6-cell Li-ion battery\\
    \hline
  \end{tabular}
\end{table}

\subsection{Software}

\begin{itemize}
	\item \textbf{Aplicación del algoritmo para aplicar los filtros al vídeo.} Para aplicar los filtros que permitan recopilar la información necesaria sobre las personas y vehículos se ha determinado que la mejor elección es \textit{OpenCV}, por ser de código abierto y por la forma en la que permite aplicar filtros, más sencilla que con cualquier otro software.
	\item \textbf{Representación de los objetos reales en el mundo virtual.} como se ha indicado anteriormente, su uso viene impuesto al presente \acs{TFG}.
	\item \textbf{Redacción de la memoria del proyecto.} Se ha decido utilizar el procesador de texto \LaTeX{} \footnote{https://www.latex-project.org/} para redactar de manera sencilla la memoria del proyecto gracias a sus características adecuadas para el desarrollo de este tipo de documentos, además de ser de código abierto.
	\item \textbf{Repositorio.}  Para el almacenamiento web de todos los archivos de la memoria así como el código fuente del proyecto se ha decidido utilizar \textit{BitBucket}\footnote{https://bitbucket.org/}, por proporcionar la posibilidad de tener repositorios privados con un máximo de 5 miembros sin necesidad de pago. También se consideró la posibilidad de utilizar \textit{GitHub}\footnote{https://github.com/}, pero al ser de pago los repositorios privados, se descartó está opción.
  \item \textbf{Sistema operativo.} Se ha escogido Linux y más concretamente Ubuntu 16.04.2 (con el entorno de escritorio MATE) por la gran cantidad de tutoriales disponibles en la red para la configuración y uso de las herramientas necesarias para la realización del proyecto.
\item \textbf{Elaboración de figuras.} Se ha decidido utilizar \textit{draw.io}\footnote{https://www.draw.io/} y \acf{GIMP}\footnote{https://www.gimp.org/} por ser ambos gratuitos y \acs{GIMP} también por ser de código abierto. El primero ofrece la posibilidad crear figuras y diagramas y exportarlos en formato \acf{XML} y el segundo permite realizar infinidad de operaciones a imágenes, como eliminar el fondo, recortarlas, escalarlas, exportarlas a otro formato, etc.
\item \textbf{Lenguaje de programación.} Ya que \textit{Blender} permite escribir scripts en \textit{Python}\footnote{https://www.python.org/}, se ha decidido escoger dicho lenguaje para programar el algoritmo de detección de objetos en movimiento así como el cliente y el servidor \textit{ZeroC Ice} y la invocación asíncrona de las operaciones de la \acs{API}.
\end{itemize}


\section{Iteraciones}\label{iteraciones}

A continuación se detallan las iteraciones del proyecto para la implementación del sistema, haciendo hincapié en los problemas encontrados así como en las posibles soluciones disponibles y la opción seleccionada.

\subsection{Iteración 1: Detección de objetos en movimiento}\label{impl-algor}

Esta iteración ha durado 2 semanas y se corresponde con la historia de usuario 1 (cuadro \ref{historia-uno}).

Aunque el generador de eventos desarrollado es capaz de analizar una cámara en tiempo real, con el fin de dedicar todos los esfuerzos al proceso de desarrollo y perfeccionamiento del sistema, se ha optado por descargar un vídeo ya grabado que fue subido por el usuario \textit{Beachfront} a la web \textit{videezy}\footnote{https://www.videezy.com/transportation/94-frogger-highway}. Dicho vídeo será analizado por el algoritmo de detección de objetos en movimiento para obtener la información de los objetos que aparezcan y que será utilizada más adelante para generar eventos.

Uno de los primeros problemas encontrados durante la realización del proyecto era determinar el mejor algoritmo para detectar los vehículos presentes en un vídeo. Las dos mejores opciones halladas fueron el basado en \textit{cascades} y el basado en eliminación del fondo. Ambos algoritmos fueron analizados en la sección \ref{detec-movi} junto con sus ventajas y desventajas. 

Una vez conocidas las ventajas e inconvenientes de cada uno de ellos, se comprobó la precisión obtenida para un mismo vídeo. En la figura \ref{algorit-comp} se muestra el mismo \textit{frame} repetido aplicando ambos algoritmos. Para ahorrar tiempo, se decidió mostrar como ejemplo de algoritmo basado en \textit{cascades} el caso desarrollado por Andrews Sobral y disponible en su repositorio de \textit{GitHub}\footnote{https://github.com/andrewssobral/vehicle\_detection\_haarcascades}, debido a que ya se disponía del archivo \acs{XML} obtenido como resultado tras la fase de entrenamiento. Cabe indicar que el vídeo utilizado para comparar los dos algoritmos de detección de objetos también ha sido obtenido de dicho repositorio. En cuanto al algoritmo basado en eliminación del fondo, se ha utilizado el escrito por Adrian Rosebrock\footnote{http://www.pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/} con algunas modificaciones realizadas por el estudiante con el fin de adaptarlo al problema que pretende resolver el proyecto realizado, para optimizar la precisión en la detección y para incluir ciertas mejoras que serán descritas posteriormente en la sección \ref{clientes-zeroc}. Como puede observarse en dicha imagen, la precisión del basado en \textit{cascades} es algo mayor, pero hay que tener en cuenta que el basado en eliminación del fondo no ha requerido ninguna clase de entrenamiento mientras que el primero sí. Por tanto, y considerando también las ventajas e inconvenientes de cada algoritmo, se ha decidido utilizar el basado en eliminación del fondo para desarrollar el ejemplo de generador de eventos que analizará un vídeo.

\begin{figure}[!h]
\centering
   \includegraphics[width=16cm]{figs/algorit-comp.png}
\caption{Comparación del algoritmo basado en \textit{cascades} y del basado en eliminación del fondo. Fuente del vídeo y de los archivos necesarios para el algoritmo basado en \textit{cascades}: \cite{gitrepository-web}}
\label{algorit-comp}
\end{figure}

Una vez implementado el algoritmo, se genera como salida un archivo cuyo contenido es una lista de jsons. Dicho archivo será utilizado por el cliente \textit{ZeroC Ice} que enviará los eventos generados por el generador implementado a modo de ejemplo al servidor.


\subsection{Iteración 2: Creación del mundo virtual}

Esta iteración ha durado 1 semana y se corresponde con la historia de usuario 2 (cuadro \ref{historia-dos}).

Una vez realizado el procesamiento del vídeo, era necesario escoger un motor gráfico que permita construir un mundo virtual y representar objetos virtuales como pueden ser peatones, vehículos o construcciones en él. El proyecto CitiSim utiliza \textit{Blender} y por lo tanto es un requisito impuesto al presente \acs{TFG}. Como vimos en la subsección \ref{motores-graficos} hay otras alternativas pero en este caso es obligatorio usar \textit{Blender}, cuya utilización permite obtener ciertas ventajas también tratadas en dicha subsección. La creación de un mundo virtual se realizó mediante un \textit{plugin} de \textit{Blender} que permite importar los datos \acs{OSM} de \acl{OSM} (explicado en la sección \ref{explicacion-osm}) de una ciudad para obtener su modelo virtual con edificios y calles.

El prototipo obtenido tras esta iteración es capaz de descargar automáticamente un fichero \acs{OSM} y, posteriormente, cargar en \textit{Blender} los datos para construir un mundo virtual. Para comprobar el correcto funcionamiento del prototipo, se ejecutó el programa \textit{Python} para descargar automáticamente el vídeo y después se utilizó el \textit{plugin} para construir el mundo.

\subsection{Iteración 3: Operaciones de la \acs{API} no distribuidas}

Esta iteración ha durado 1 semana y se corresponde con la historia de usuario 3 (cuadro \ref{historia-tres}).

Antes de comenzar con el desarrollo distribuido de las operaciones, se decidió empezar implementándolas para que pudieran utilizarse de forma no distribuida con el fin de conocer si las operaciones funcionan correctamente de acuerdo a lo especificado por el cliente.

El prototipo obtenido tras esta iteración tiene implementadas las operaciones requeridas por el cliente. Se comprobó el correcto funcionamiento de todas las operaciones ejecutando casos de prueba con valores ficticios.


\subsection{Iteración 4: Operaciones de la \acs{API} distribuidas y síncronas}\label{cliente-zeroc}

Esta iteración ha durado 2 semanas y se corresponde con la historia de usuario 4 (cuadro \ref{historia-cuatro}).

Antes de poder representar los diferentes eventos en el mundo virtual, era necesario crear el servidor \textit{ZeroC Ice} y modificar las operaciones que van a ser invocadas por los distintos clientes. Para ello, lo primero fue escribir en lenguaje \textit{Slice} las definiciones de las operaciones que conforman la \acs{API}. Después, se escribió el código necesario tanto para la creación del servidor como para la actualización de todas y cada una de las operaciones con el fin de permitir su invocación distribuida.  

Una vez actualizadas las operaciones provistas por el servidor \textit{ZeroC Ice}, se desarrolló un cliente capaz de invocar dichas operaciones para comprobar el correcto funcionamiento de cada una de ellas. Por tanto, se escribió el código necesario para crear el cliente y también se implementaron funciones que realizaban invocaciones a cada una de las operaciones.

El prototipo obtenido permite invocar de forma distribuida las operaciones síncronas de la \acs{API}. Para comprobar el correcto funcionamiento de las operaciones se utilizó el cliente de prueba que se ha comentado antes.


\subsection{Iteración 5: Operaciones de la \acs{API} distribuidas y asíncronas}\label{invoc-asinc}

Esta iteración ha durado 2 semanas y se corresponde con la historia de usuario 5 (cuadro \ref{historia-cinco}).

Debido a las ventajas que se obtienen al hacer uso de invocaciones asíncronas de las distintas operaciones frente a las invocaciones síncronas, se optó por desarrollar las invocaciones asíncronas de las operaciones ya implementadas una vez comprobado su correcto funcionamiento mediante el cliente de prueba. La principal ventaja de utilizar invocaciones asíncronas es que el cliente puede realizar la invocación de una operación deseada pero no es necesario que espere hasta que el servidor la complete, por lo que puede realizar otras invocaciones o realizar otras tareas de forma simultánea a la finalización de la operación. Relacionada con la anterior se encuentra la siguiente ventaja: el uso de los denominados \textit{callbacks} o retrollamadas, que pueden ser utilizadas por el servidor \textit{ZeroC Ice} para contactar con el cliente para diversos fines: para enviarle el resultado de una operación invocada y/o decirle si ésta se ha realizado correctamente o no, para enviarle información solicitada, etc. Por tanto, hubo que escribir las funciones que iban a encargarse de controlar la invocación asíncrona además de actualizar la implementación de las operaciones de la \acs{API}. Además, se modificó el cliente de prueba para realizar las invocaciones de las operaciones asíncronas, y no de las síncronas.

El prototipo obtenido permite a uno o más clientes \textit{ZeroC Ice} invocar de forma distribuida las operaciones asíncronas de la \acs{API}. Al igual que en la iteración anterior, se utilizó el cliente de prueba para comprobar el correcto funcionamiento de todas las operaciones.


\subsection{Iteración 6: Virtualización de la plataforma}

Esta iteración ha durado 1 semana y se corresponde con la historia de usuario 6 (cuadro \ref{historia-seis}).

Tras la realización de la iteración anterior, el siguiente paso fue la virtualización de la plataforma. Lo primero de todo fue elegir el software proveedor más adecuado así como también el software para la creación y gestión de máquinas virtuales y para el aprovisionamiento de la máquina virtual, por lo que se realizó un análisis de las distintas opciones encontradas para los tres casos. El resultado de dicho análisis fue la elección de \textit{Vagrant} para la creación de la máquina virtual, de \textit{Virtualbox} como software proveedor y de \textit{Ansible} para la instalación y configuración de las herramientas utilizadas durante el desarrollo de la plataforma, y puede verse en las subsecciones \ref{soft-mv}, \ref{soft-proveedor} y \ref{soft-aprov}. Por último, fue necesario escribir el archivo de \textit{Vagrant} para la configuración de la máquina virtual y el archivo de \textit{Ansible} para la instalación de las herramientas.

El prototipo obtenido es un entorno virtual con todas las herramientas y librerías necesarias así como con la estructura de directorios que se ha utilizado para el desarrollo del proyecto. Para comprobar que se ha realizado correctamente todo el proceso de virtualización, se probó a ejecutar el servidor y el cliente \textit{ZeroC Ice} y se realizó la invocación de una operación de la \acs{API}.


\subsection{Iteración 6: Validación del sistema}

Esta iteración ha durado 1 semana y no se corresponde con ninguna historia de usuario.

Se desplegó la plataforma haciendo uso del software de gestión de máquinas virtuales escogido y se comprobó tanto la correcta instalación de las herramientas, paquetes y librerías necesarias realizada mediante el software de aprovisionamiento,  como el buen funcionamiento de las distintas partes que componen la arquitectura del sistema, detalladas en el capítulo \ref{chap:arquitectura}. Para llevar a cabo dicha comprobación, se procedió a la realización de diferentes casos de estudio haciendo uso del cliente \textit{ZeroC Ice} capaz de invocar las distintas operaciones del servidor, y del generador de eventos que analiza un vídeo para detectar peatones y vehículos. Los resultados obtenidos tras la realización de dichos casos de estudio pueden verse en la sección \ref{ejemplos-prueba}.


\section{Historias de usuario}

A continuación se muestran diferentes cuadros con las historias de usuario que se han obtenido durante la realización del proyecto:

\input{tables/historia-uno.tex}
\input{tables/historia-dos.tex}
\input{tables/historia-tres.tex}
\input{tables/historia-cuatro.tex}
\input{tables/historia-cinco.tex}
\input{tables/historia-seis.tex}


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
