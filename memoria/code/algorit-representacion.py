	def cameraEventGenerator(self):
		data = []
		
		with open('roadData.txt') as f1:
			for line in f1:
				roadData = line.split(",")

			listOfIndexes = [0] * int(roadData[1])
			listOfCubes = [[None] for i in range(int(roadData[0]))] 

		with open('data.json') as f: 
			data = json.load(f)

		while len(data['objects']) > 0:
			frame = data['objects'].pop(0)

			while (len(data['objects']) > 0) and ((data['objects'][0])['class'] == 'Vehicle'):
				v = data['objects'].pop(0)
				index = listOfIndexes[v['lane']]
			if (((listOfCubes[v['lane']])[index]) is None):          
				self.paintVehicleByLocationNameExample()
				((listOfCubes[v['lane']])[index]) = True
				self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
				lastXCoord = v['xCoord']
				listOfIndexes[v['lane']] += 1
				if listOfIndexes[v['lane']] >= int(roadData[0]):
					listOfIndexes[v['lane']] = 0
			else:
				if(v['xCoord'] > lastXCoord):
					self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
					lastXCoord = v['xCoord']
					listOfIndexes[v['lane']] += 1
					if listOfIndexes[v['lane']] >= int(roadData[0]):
						listOfIndexes[v['lane']] = 0
				else:
					self.destroyObjectExample()
					if int(roadData[0]) == 1:
						self.paintVehicleByLocationNameExample()
					self.visualizer.moveObject("Car", geva.Point3D(v['xCoord']/10,0,0))
					lastXCoord = v['xCoord']
