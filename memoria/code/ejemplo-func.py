def setup(conffile="../conf/detection.cfg"):
    configuration = configparser.ConfigParser()
    configuration.read(conffile)
    return configuration

def getDeltaThresh(configobj):
	return int(configobj.get('detection_parameters', 'delta_thresh'))
