\chapter{Resultados}
\label{chap:resultados}

\noindent
\drop{E}{n} este capítulo, se dará un resumen del resultado obtenido tras la realización del proyecto y se mostrarán diferentes ejemplos de uso de la plataforma para comprobar el correcto funcionamiento de las distintas partes que la componen y que fueron explicadas detalladamente en el capítulo \ref{chap:arquitectura}.

Tras la realización del presente \acs{TFG}, se ha obtenido como resultado una plataforma capaz de recrear de forma virtual los distintos elementos de una ciudad real (como puede ser un edificio, una vía, un río,...) mediante datos \acs{OSM} para, posteriormente, representar diferentes eventos que suceden en dicha ciudad en su correspondencia virtual. Para ello, se ha desarrollado un sistema distribuido mediante el \textit{middleware} \textit{ZeroC Ice} en el que los sensores, actuadores, cámaras y teléfonos móviles entre otros, serían los clientes que invocan las operaciones que el servidor pone a su disposición para que éste represente los eventos en el mundo virtual. Para la representación, se hace uso de una \acs{API} que abstrae a los clientes de la manera en la que la herramienta gráfica escogida realiza las operaciones. Los clientes únicamente deben preocuparse de transmitir la información requerida por el servidor.

Con el fin de comprobar el correcto funcionamiento de las distintas operaciones que conforman la \acs{API}, se ha escrito un cliente \textit{ZeroC Ice} que implementa invocaciones a dichas operaciones simulando ejemplos de uso.

Por último, para garantizar la portabilidad, escalabilidad y seguridad de la plataforma, se ha implementado el despliegue automático de la misma. Para ello, se ha hecho uso de \textit{Vagrant} como software de creación y gestión de máquinas virtuales para la virtualización de la plataforma. Además, se ha utilizado \textit{Ansible} como software de aprovisionamiento para la instalación y configuración de las herramientas necesarias y \textit{VirtualBox} como software proveedor para los recursos hardware asignados a la máquina virtual. 


\section{Ejemplos para cada parte de la arquitectura}\label{ejemplos-prueba}

En esta sección se aportan diferentes ejemplos de uso de cada parte de la arquitectura con el fin de mostrarlas en funcionamiento y para comprobar que éste sea correcto. Cabe indicar que para el caso de la representación de eventos en el mundo virtual se han juntado las partes de clientes \textit{ZeroC Ice} con la del servidor para poder mostrar ejemplos y que se han descargado de la web \textit{Blendswap}\footnote{https://www.blendswap.com/} un modelo de un coche\footnote{https://www.blendswap.com/blends/view/77645} y el de una carretera\footnote{https://www.blendswap.com/blends/view/74600} para mostrar que pueden importarse modelos en el mundo virtual cuando se hace uso de invocaciones distribuidas. 


\subsection{Obtención de información del mundo real y algoritmo de detección de objetos en movimiento}\label{ejemplo-uno}

Para este ejemplo se va a utilizar un vídeo previamente grabado disponible en la web \textit{videezy}\footnote{https://www.videezy.com/transportation/94-frogger-highway} y que fue subido por el usuario \textit{Beachfront}. En la figura \ref{algorit-example} puede verse un \textit{frame} del vídeo en el que los objetos presentes en él han sido detectados, y también los vehículos de dicho \textit{frame} representados en el mundo virtual.

\begin{figure}[!h]
\centering
   \includegraphics[width=17cm]{figs/algorit-example.png}
\caption{\textit{Frame} con vehículos detectados por el algoritmo de detección y dichos vehículos representados en el mundo virtual. Fuente del vídeo: \cite{trafficvideo-web}}
\label{algorit-example}
\end{figure}

La información del mundo real obtenida sería la referente a los vehículos detectados y que ha sido almacenada en un fichero en formato \acs{JSON}. El listado \ref{frames-deteccion} muestra la información de dos \textit{frames} del vídeo así como de los vehículos detectados pertenecientes a ellos.

\lstinputlisting[caption={Información de los dos primeros \textit{frames} del vídeo en formato \acs{JSON}},label={frames-deteccion}]{code/frames-deteccion.json}


\subsection{Clientes y servidor ZeroC Ice: Invocación de las operaciones}

Para mostrar el funcionamiento de un cliente y un servidor \textit{ZeroC Ice}, se va a mostrar el resultado de invocar algunas de las operaciones que componen la \acs{API} de GEVA por parte del cliente desarrollado para probar el correcto funcionamiento de ellas y que fueron explicadas en la sección \ref{servidor-zeroc}. En todos los ejemplos que se describan a continuación se utilizará como mundo virtual el campus de Ciudad Real.

\subsubsection{Representación de un incendio y su área de propagación en el campus de Ciudad Real}

En este primer ejemplo van a utilizarse dos operaciones: \textit{paintDetectedEventByCoords} para pintar un incendio y \textit{paintCircularAreaByCoords} para pintar su área de propagación. La figura \ref{area-example} muestra el resultado de la invocación de ambas operaciones.

\begin{figure}[!h]
\centering
   \includegraphics[width=15.5cm]{figs/area-example.png}
\caption{Representación de un incendio y su área de propagación en el campus de Ciudad Real}
\label{area-example}
\end{figure}


\subsubsection{Representación de un vehículo circulando por el campus de Ciudad Real}

Para este segundo ejemplo se va a hacer uso de la operación \textit{paintVehicleByCoords} que permite representar un vehículo en el mundo virtual mediante sus coordenadas en él. El resultado de la invocación de dicha operación puede verse en la figura \ref{vehicle-example}.

\begin{figure}[!h]
\centering
   \includegraphics[width=15.5cm]{figs/vehicle-example.png}
\caption{Representación de un vehículo circulando por el campus de Ciudad Real}
\label{vehicle-example}
\end{figure}


\subsubsection{Representación de una ruta en el campus de Ciudad Real}

Para este tercer ejemplo, se ha utilizado la operación \textit{paintRouteByCoords} para pintar una ruta indicando las coordenadas 3D de los puntos que componen la ruta. La figura \ref{route-example} muestra el resultado de pintar una ruta.

\begin{figure}[!h]
\centering
   \includegraphics[width=15.5cm]{figs/route-example.png}
\caption{Representación de una ruta en el campus de Ciudad Real}
\label{route-example}
\end{figure}

\subsubsection{Representación de un área irregular en el campus de Ciudad Real}

En este cuarto ejemplo, va a utilizarse la función \textit{paintDesiredAreaByCoords} que permite pintar un polígono irregular para simular un área. La figura \ref{desiredArea-example} muestra un área irregular representada.

\begin{figure}[!h]
\centering
   \includegraphics[width=15.5cm]{figs/desiredArea-example.png}
\caption{Representación de un área irregular en el campus de Ciudad Real}
\label{desiredArea-example}
\end{figure}
 
\subsubsection{Representación en el mundo virtual de los objetos detectados}\label{ejemplo-cinco}

En este último ejemplo, se ha utilizado la función del cliente \textit{cameraEventGenerator} que procesa los datos en formato \acs{JSON} obtenidos en el primer ejemplo de este capítulo y utiliza las siguientes funciones del servidor para representar en el mundo virtual de \textit{Blender} los vehículos detectados:

\begin{itemize}
\item \textit{paintVehicleByLocationName}: para crear un vehículo en el mundo virtual en la posición de un objeto de \textit{Blender} de tipo \textit{Arrows} llamado \textit{spawner}.
\item \textit{moveObject}: para mover el vehículo detectado desde su anterior posición hasta la nueva.
\end{itemize}

Puede verse en la figura \ref{algorit-example} los dos vehículos representados en el mundo virtual.

\subsection{Virtualización de la plataforma}

Para mostrar la correcta virtualización de la plataforma, pueden verse los archivos \textit{Vagrantfile} para la creación y gestión de la máquina virtual y \textit{playbook.yml} para la instalación de las distintas herramientas y librerías necesarias en el proyecto en la sección \ref{archivos-virt} del Anexo \ref{instalacion} una vez se han seguido las instrucciones indicadas en el mismo. Además, la figura \ref{mv-running} muestra el mundo virtual en \textit{Blender} utilizado en los distintos ejemplos de este capítulo.

\begin{figure}[!h]
\centering
   \includegraphics[width=18cm]{figs/mv-running.png}
\caption{Correspondencia virtual del campus de Ciudad Real en la máquina virtual}
\label{mv-running}
\end{figure}


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
