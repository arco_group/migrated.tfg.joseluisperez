\chapter{Arquitectura del sistema}
\label{chap:arquitectura}

\noindent
\drop{E}{n} este capítulo se describe la arquitectura del sistema obtenido tras la realización de las iteraciones detalladas en la sección \ref{iteraciones}. Puede verse en la figura \ref{arquitectura} que dicha arquitectura consta de tres partes: 

\begin{figure}[!h]
\centering
   \includegraphics[width=16cm]{figs/arquitectura.png}
\caption{Arquitectura del sistema desarrollado}
\label{arquitectura}
\end{figure}

\section{Obtención de información del mundo real}\label{primera-parte}

La primera parte es la referente al mundo real. A lo largo del tiempo ocurrirán diferentes eventos que deberán ser captados por distintos sensores, cámaras, etc. El caso más destacado es la implementación del generador de eventos cuya finalidad es analizar una cámara para detectar peatones y vehículos en movimiento y generar posteriormente un fichero de datos en formato \acs{JSON} (explicado en la sección \ref{explicacion-json}) que será utilizado por un cliente \textit{ZeroC Ice} para generar eventos y enviarlos al servidor \textit{ZeroC Ice} para su representación en el mundo virtual. Por ese motivo se representa en la figura una cámara real en la parte del mundo real, aunque pueden ser otros elementos como sensores, actuadores, etc (como se verá más adelante). Cómo se comentó anteriormente en la subsección \ref{impl-algor}, el algoritmo de detección escogido es el basado en la eliminación del fondo y el código base utilizado para su implementación es el escrito por Adrian Rosebrock\footnote{http://www.pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/}. También se comentó la realización de diversas modificaciones realizadas con el fin de adaptar el código al problema que pretende resolver el proyecto realizado, para optimizar la precisión en la detección y para incluir ciertas mejoras. Dichas modificaciones son las siguientes:

\begin{itemize}
\item disponer de un archivo con la configuración en lugar de establecer los parámetros en las distintas líneas de código. Dicho archivo tiene como nombre \textit{detection.cfg}. Los parámetros actualmente configurables son la activación o desactivación de las ventanas que muestran el \textit{threshold}, el número de carriles que conforman la carretera, el incremento en el umbral y el área mínima que debe alcanzarse para considerar un objeto detectable, siendo los dos últimos parámetros los que deben ser modificados para ajustar la precisión en la detección. Para obtener la configuración de dicho archivo se utiliza el módulo \textit{configparser} proporcionado por \textit{Python}. Para utilizar este módulo es necesario disponer de dos ficheros auxiliares: uno que contiene la configuración, llamado \textit{detection.cfg} en este proyecto, y otro llamado \textit{config.py} en el que se han escrito funciones encargadas de parsear, si fuera necesario, los datos leídos del archivo de configuración por la función \textit{setup}, antes de pasárselos al programa \textit{Python} (en este caso el que implementa el algoritmo de detección) que ha invocado las funciones del archivo \textit{config.py}. El listado \ref{ejemplo-config} muestra como ejemplo el archivo de configuración del algoritmo de detección y el listado \ref{ejemplo-func} muestra a modo de ejemplo las funciones \textit{setup} y \textit{getDeltaThresh}, siendo esta última utilizada para obtener el umbral máximo permitido. En el primer listado de los dos indicados puede observarse la sintaxis de un archivo de configuración del módulo \textit{configparser} en la que debe indicarse mediante una cadena entre corchetes el nombre de cada sección. y en la que cada atributo tiene un valor asignado mediante un igual. Por último, la figura \ref{configparser} muestra el esquema seguido por el algoritmo de detección para obtener la configuración, siendo el fichero con nombre \textit{object-tracker.py} el programa \textit{Python} que implementa el algoritmo de detección basado en la eliminación del fondo. Aunque la configuración actual del algoritmo de detección de objetos en movimiento es bastante corta, si en un futuro se quisieran añadir nuevas configuraciones sería muy fácil hacerlo, ya que únicamente habría que modificar el archivo \textit{config.py} para añadir las funciones encargadas de obtener los nuevos atributos escritos en el archivo \textit{detection.cfg}. Cabe indicar que esta forma de gestionar y llevar a cabo la configuración puede extenderse al resto del proyecto de forma sencilla si fuera necesario.
\item Los datos de los vehículos detectados se envían a un archivo llamado \textit{data.json}. Uno de los problemas encontrados durante la obtención de los datos de una cámara ha sido determinar una forma correcta para su almacenamiento persistente. Dicho problema será analizado en la sección \ref{dificultades}.
\item También se guardan los datos del vídeo procesado en el archivo \textit{road.txt}. Dichos datos son el número máximo de vehículos que aparecen en un frame y el número de carriles.
\item Uso de una función para ordenar de mayor a menor los vehículos detectados según su posición en el eje X, ya que el algoritmo los detecta en cualquier orden y no hace uso de un identificador único que permita conocer en todo momento de qué vehículo son los datos (este problema ha sido comentado con más detalle en la sección \ref{dificultades}).
\end{itemize}

\lstinputlisting[caption={Archivo de configuración del algoritmo de detección de objetos en movimiento basado en eliminación del fondo},label={ejemplo-config}]{code/ejemplo-config.cfg}
\lstinputlisting[caption={Función \textit{setup} que lee la configuración y función \textit{getDeltaThresh}, que recupera el umbral máximo permitido},label={ejemplo-func}]{code/ejemplo-func.py}

\begin{figure}[!h]
\centering
   \includegraphics[width=16cm]{figs/configparser.png}
\caption{Esquema de la obtención de la configuración por parte del algoritmo de detección}
\label{configparser}
\end{figure}

Es necesario indicar que el algoritmo también permite procesar directamente el vídeo obtenido en tiempo real mediante una cámara o una webcam, por lo que puede usarse por ejemplo junto con una Raspberry Pi. También cabe indicar que el volcado de los datos a un fichero en formato \acs{JSON} es un paso intermedio, y en el futuro el algoritmo mandará directamente los datos al servidor \textit{ZeroC Ice} sin tener que leer el archivo \textit{data.json}.

En la figura \ref{ejemplo-deteccion} puede verse un ejemplo de vehículo detectado mediante el algoritmo basado en eliminación del fondo una vez realizadas todas las modificaciones anteriores.

\begin{figure}[!h]
\centering
   \includegraphics[width=17cm]{figs/ejemplo-deteccion.png}
\caption{Vehículo detectado mediante el algoritmo basado en eliminación del fondo implementado. Fuente del vídeo: \cite{trafficvideo-web}}
\label{ejemplo-deteccion}
\end{figure}

El pseudocódigo del algoritmo final desarrollado tras llevar a cabo las modificaciones antes comentadas puede verse en el algoritmo \ref{pseudo}. Cabe indicar que si se quiere detectar vehículos, es necesario indicar el número de carriles que tiene la carretera y también debe determinarse el carril en el que se encuentra el vehículo mediante la coordenada Y obtenida cada vez que se detecta un objeto mediante el algoritmo.

\begin{algorithm}[!h]
\label{pseudo}
 \caption{Pseudocódigo del algoritmo de detección de objetos basado en eliminación del fondo}
     \SetAlgoLined
     Importación de las librerías necesarias\;
     Inicialización de variables\;
     Obtener configuración\;
     Obtener el vídeo de entrada\;
     \While{seguir = True}{
        obtener frame\;
        \If{frame no obtenido}{
          salir\;
        }

        pasar el frame a escala de grises y reescalar\;

        \If{la media es None}{
          inicializar media\;
        }

        acumular la media entre los frames anteriores y el actual\;
        realizar la diferencia entre el frame actual y la media\;
        calcular el umbral y obtener los contornos del frame actual\;

        \ForEach{contorno en contornosFrame}{
          \eIf{área del contorno < minÁrena}{
            saltar\;
          }{
            obtener el rectángulo del contorno\;
            dibujar el rectángulo en el frame\;
            guardar el vehículo junto con sus datos en listaVehículos\;
	        }
        }
        \If{tamaño de listaVehículos > 0}{
          ordenar vehículos de mayor a menor por su coordenada X\;
          almacenar en el fichero de datos\;
        }
        mostrar el frame en el vídeo\;
        comprobar si se ha pulsado alguna tecla\;
        \If{teclaPulsada = q}{
          salir del bucle principal\;
        }
     
     }
     cerrar fichero de datos\;
     cerrar todos las ventanas abiertas\;
     \BlankLine
     \BlankLine
     \BlankLine

\end{algorithm}

Como bien se ha comentado a lo largo del presente documento, el algoritmo, además de detectar vehículos, también es capaz de detectar personas, como puede verse en la figura \ref{deteccion-personas}. El vídeo utilizado para este ejemplo ha sido descargado de la web \textit{videoblocks}\footnote{https://www.videoblocks.com/video/high-angle-shot-of-pedestrians-crossing-the-street-on-zebra-on-green-light-ctew8rt/}.

\begin{figure}[!h]
\centering
   \includegraphics[width=12cm]{figs/deteccion-personas.png}
\caption{Ejemplo de detección de un peatón. Fuente del vídeo: \cite{pedestrianvideo-web}}
\label{deteccion-personas}
\end{figure}


\section{Clientes ZeroC Ice}\label{clientes-zeroc}

La segunda parte es la formada por los diferentes clientes \textit{ZeroC Ice}, que envían eventos mediante la invocación de las distintas operaciones provistas por el servidor mediante la \acs{API} de GEVA. Como se ha comentado anteriormente en la subsección \ref{cliente-zeroc}, para comprobar el correcto funcionamiento de las distintas operaciones que conforman la \acs{API}, se ha implementado un cliente que genera eventos ficticios realizando invocaciones al servidor \textit{ZeroC Ice}. En una ciudad real, los clientes \textit{ZeroC Ice} serían los distintos sensores, actuadores, cámaras, teléfonos móviles, etc, capaces de captar y/o generar eventos ocurridos en dicha ciudad, como puede verse en la figura \ref{city-events}. 

\begin{figure}[!h]
\centering
   \includegraphics[width=18cm]{figs/city-events.png}
\caption{Eventos ocurridos en el mundo real y representados en su correspondencia virtual}
\label{city-events}
\end{figure}

Como ejemplo representativo de ésto, se ha desarrollado un cliente \textit{ZeroC Ice} que procesa la información obtenida y almacenada en el fichero \textit{data.json} por el algoritmo de detección de objetos explicado en la primera parte de la arquitectura (sección \ref{primera-parte}). Dicho procesamiento consiste en ir obteniendo los objetos ordenados de mayor a menor en función de su coordenada X en cada \textit{frame} (aunque el algoritmo permite detectar peatones y vehículos, a partir de este momento se va a utilizar en vez del término objeto el de vehículo, ya que en el ejemplo documentado se han detectado únicamente vehículos). Si dicha coordenada es mayor que la última procesada, significa que todavía se sigue tratando el mismo vehículo, por lo que se invoca la operación que permite mover un objeto. Si fuera al revés, significa que se tiene que empezar a tratar otro vehículo nuevo, por lo que se tienen que invocar dos funciones: la primera para borrar el vehículo actual y la segunda para crear el nuevo. Como se ha comentado en la sección anterior, la necesidad de manejar las coordenadas X de los vehículos viene impuesta debido a que \textit{OpenCV} no maneja identificadores y además, procesa los vehículos de cada \textit{frame} en un orden aleatorio. El listado \ref{algorit-representacion} muestra el algoritmo utilizado por el cliente \textit{ZeroC Ice} para realizar el procesamiento de la forma que se acaba de explicar. Cabe indicar que se hace uso de una lista para manejar los carriles (o aceras en el caso de haber detectado peatones) y otra lista de listas para manejar los vehículos correspondientes a cada carril (o los peatones que circulan por cada acera). La primera lista contiene los índices de los respectivos carriles que se utilizan para conocer qué vehículo debe procesarse de los asignados a ese carril (posición dentro de uno de las listas que están dentro de la segunda lista). Una vez procesado cada vehículo, el cliente generará eventos y los enviará al servidor para su representación en el mundo virtual. 

Puede verse en las secciones \ref{ejemplo-uno} y \ref{ejemplo-cinco} un ejemplo de utilización del algoritmo de detección y la posterior representación en el mundo virtual de los vehículos detectados.

\lstinputlisting[caption={Algoritmo usado para procesar los vehículos del archivo \textit{data.json} y generar los correspondientes eventos},label={algorit-representacion}]{code/algorit-representacion.py}



\section{Servidor ZeroC Ice y representación de eventos en el mundo virtual}\label{servidor-zeroc}

La tercera y última parte de la arquitectura es la correspondiente a la representación de los eventos generados anteriormente en el mundo virtual. El servidor \textit{ZeroC Ice} recibe invocaciones de sus operaciones por parte de los clientes y representa en \textit{Blender} los eventos recibidos. 

Las operaciones invocables del servidor se han definido en lenguaje \textit{Slice} en el archivo \textit{geva.ice}. Dichas operaciones son las siguientes:

\begin{itemize}
\item \textbf{\textit{paintDetectedEventByCoords}}: usada cuando se quiere pintar un evento como, por ejemplo, un incendio. Simplemente es necesario indicar el nombre del evento, el modelo utilizado para representarlo y la localización.
\item \textbf{\textit{paintDetectedEventByLocationName}}: tiene la misma finalidad que la anterior, pero en vez de indicar las coordenadas en las que se quiere pintar el objeto, hay que especificar el nombre del objeto en cuyas coordenadas queremos representar el evento. Por ejemplo, dicho objeto puede ser un edificio.
\item \textbf{\textit{paintVehicleByCoords}}: permite pintar un vehículo en una localización dada indicando también el nombre que se quiere asignar a dicho vehículo, el modelo que se le quiere asignar y la localización.
\item \textbf{\textit{paintVehicleByLocationName}}: igual que la anterior pero en vez de especificar las coordenadas hay que indicar el objeto en cuya posición se quiere pintar el vehículo. Dicho objeto puede ser, por ejemplo, una calle. 
\item \textbf{\textit{paintPersonByCoords}}: utilizado para pintar una persona indicando la misma información que para el caso de la operación \textit{paintVehicle}. Se ha decidido separar esta función de las anteriores debido a que el cliente \textit{ZeroC Ice} no tiene por qué saber que una misma función puede representar distintos objetos.
\item \textbf{\textit{paintPersonByLocationName}}: misma finalidad que la anterior, aunque se diferencian en que en ésta es necesario indicar el objeto en cuya localización se quiere pintar la persona. Al igual que con la función \textit{paintVehicleByLocationName}, dicho objeto puede ser, por ejemplo, una calle.
\item \textbf{\textit{paintRouteByCoords}}: pinta una ruta indicando el nombre que se le quiere asignar así como una lista de puntos 3D que la definen. Podría utilizarse esta función si, por ejemplo, se quisieran indicar las rutas que sigue un módulo de transportes.
\item \textbf{\textit{paintRouteByNames}}: misma finalidad que la anterior, diferenciándose de ella en que en este caso la ruta está delimitada por los identificadores de los objetos (edificios, calles, ...) que definen la ruta.
\item \textbf{\textit{paintServiceByCoords}}: se utiliza para pintar un servicio (por ejemplo un edificio con wifi pública) indicando el identificador (el proxy al servicio), el modelo a asignar y la localización.
\item \textbf{\textit{paintServiceByLocationName}}: igual que el anterior pero en esta ocasión no se especifican directamente las coordenadas en las que se quiere pintar el servicio, sino el objeto cuya localización es la deseada también para el servicio.
\item \textbf{\textit{paintCircularAreaByCoords}}: permite pintar un área circular. Para ello, es necesario indicar el nombre, el color, la localización del centro del círculo y su radio. Puede utilizarse para, por ejemplo, delimitar barrios, áreas cortadas o áreas que cubre un sensor.
\item \textbf{\textit{paintCircularAreaByLocationName}}: su función es la misma que la del anterior, aunque en vez de especificar las coordenadas se indica el identificador del objeto en cuyas coordenadas se quiere pintar el área (el centro).
\item \textbf{\textit{paintDesiredAreaByCoords}}: parecida a la función \textit{paintCircularAreaByCoords}, pero en vez de indicar el radio es necesario indicar una lista de vértices (puntos 3D) en el mismo orden en el que queremos que estén conectados. Podría utilizarse para las mismas situaciones que la operación \textit{paintCircularArea}.
\item \textbf{\textit{paintDesiredAreaByLocationName}}: su finalidad es la misma que para la anterior, cambiando únicamente la indicación directa de la localización por la especificación del objeto que está situado en la misma posición que la que tendrá el área deseada.
\item \textbf{paintSensorByCoords}: utilizado para representar un tipo de sensor como, por ejemplo, uno capaz de obtener la temperatura. Es necesario indicar el identificador (el proxy al sensor), el modelo utilizado para representarlo, la localización y una lista de puntos 3D que simulará la cobertura del sensor.
\item \textbf{\textit{paintSensorByLocationName}}: exactamente igual que la anterior pero en vez de especificar la localización se indica el identificador del objeto en cuya posición se quiere colocar también el sensor. 
\item \textbf{\textit{eventGenerator}}: permite simular el movimiento de cualquier objeto del mundo virtual, por ejemplo un vehículo. Es la operación utilizada para probar el generador de eventos que analiza un vídeo para detectar vehículos y que ha sido explicado en la sección anterior. 
\item \textbf{\textit{setObjectLocation}}: posibilita cambiar la localización de un objeto indicando simplemente su nombre y la nueva localización.
\item \textbf{\textit{setObjectRotation}}: sirve para modificar la rotación de un objeto indicando su nombre y una lista con los ángulos que debe estar rotado, uno con respecto al eje X, otro con respecto al eje Y y el último con respecto al eje Z. Cabe indicar que aunque \textit{Blender} asume que los ángulos están en radianes, cuando se invoque esta operación, deben estar en grados.
\item \textbf{\textit{joinObjects}}: permite unir dos o más objetos simplemente indicando los nombres que se les ha asignado al ser pintados. Es muy útil si, por ejemplo, se quiere unir un sensor con su área, ya que al ser pintados de forma independiente, las transformaciones aplicadas a uno no se propagarán a otro a menos que estén unidos. 
\item \textbf{\textit{setActiveCamera}}: sirve para cambiar la cámara activa del mundo virtual. Puede utilizarse si, por ejemplo, existen distintas cámaras de tráfico y quiere ``pincharse'' una de ellas.
\item \textbf{\textit{changeObjectIdentifier}}: utilizada para cambiar el nombre de un objeto. Para ello, es necesario indicar el nombre actual del objeto y el nuevo nombre deseado.
\item \textbf{\textit{scaleObject}}: escala un objeto ya existente en el mundo virtual indicando su nombre y la escala en los ejes X, Y y Z. Debe indicarse 0,001 en el eje o ejes en los que no se quiera aplicar esta operación.
\item \textbf{\textit{setObjectVisibility}}: oculta o muestra un objeto del mundo virtual indicando su nombre y un valor booleano con la opción deseada: \textit{True} si se quiere ocultar el objeto y \textit{False} si se quiere que sea mostrado.
\item \textbf{\textit{destroyObject}}: destruye un objeto indicado mediante su identificador.
\item \textbf{\textit{destroyAllObjects}}: destruye todos los objetos que estén únicamente en la capa uno de \textit{Blender} por dos motivos: uno porque en las demás capas se encuentran los modelos que se utilizan como base para generar eventos; y dos, porque es en esta capa dónde se va a construir por defecto el mundo virtual. 
\item \textbf{\textit{setObjectColor}}: permite establecer el color de un objeto indicando su identificador y una cadena con el color deseado. Ya que para establecer un color en \textit{Blender} se utilizan tres números que varían también según la intensidad y un usuario puede no saber la combinación correcta para un determinado color, se ha decidido que el usuario especifique el color y si está soportado por el servidor, se pintará el objeto con ese color, en caso contrario, se pintará con un color por defecto. Actualmente, los colores soportados son los siguientes: rojo, amarillo, azul, verde, negro, blanco, naranja y violeta. 
\item \textbf{\textit{accessSensor}}: permite acceder a un sensor indicando su identificador (o proxy). 
\item \textbf{moveObject}: mueve un objeto a una nueva posición indicando el identificador del objeto y la nueva posición. Aunque puede parecer que esta función realiza la misma operación que \textit{setObjectLocation}, en parte es cierto, aunque la primera da la sensación de movimiento y la segunda no ya que establece directamente la nueva localización.
\end{itemize}

Puede verse el \textit{Slice} completo en el listado \ref{slice}.
\lstinputlisting[caption={Definición de las distintas operaciones invocables en lenguaje \textit{Slice}},label={slice}]{code/geva.ice}

Aunque éstas son las operaciones que pueden ser invocadas por los clientes \textit{ZeroC Ice}, la \acs{API} está compuesta también por algunas funciones privadas, que implementan parte de la lógica derivada de las operaciones invocadas por los clientes y que puede ser requerida por parte de más de una operación. Por ejemplo, todas las operaciones que reciben el nombre del modelo que debe utilizarse cuando se pinte un nuevo objeto o evento tienen que acceder primero a dicho modelo, por lo que se ha implementado la búsqueda de objetos ya existentes en el mundo virtual mediante su identificador de forma privada.

Para la invocación asíncrona, se ha utilizado la estructura de datos de tipo \textit{queue} o cola para almacenar en ella las distintas invocaciones realizadas por los clientes, permitiéndoles seguir realizando otras invocaciones o tareas propias. El servidor consulta la cola para comprobar si hay invocaciones nuevas. Si es así, retira de la cola una invocación y la procesa. Si, por el contrario, no hubiera ninguna nueva, esperaría un tiempo antes de volver a realizar la comprobación.

En la figura \ref{diag-secuencia} puede verse un ejemplo de invocación distribuida asíncrona de una operación llamada A de la \acs{API} por parte de un cliente.

\begin{figure}[!h]
\centering
   \includegraphics[width=16cm]{figs/diag-secuencia.png}
\caption{Invocación distribuida asíncrona de una operación de la \acs{API}}
\label{diag-secuencia}
\end{figure}



\subsection{Creación del mundo virtual}

Para la creación del mundo virtual se utilizó un \textit{plugin} desarrollado por Vladimir Elistratov\footnote{https://github.com/meta-androcto/blenderpython/blob/master/scripts/addons\_extern/io\_import\_scene\_osm.py.py} que permite representar en \textit{Blender} una ciudad o parte de ella definida mediante elementos de \acl{OSM} (explicados en la sección \ref{openstreetmap}) manejando una clase llamada \textit{TransverseMercator} escrita también por Vladimir Elistratov y encargada de convertir las coordenadas especificadas mediante latitud y longitud en los ficheros .osm en coordenadas X, Y y Z globales que puede manejar \textit{Blender}.

Puede verse en la figura \ref{blender-osm} la representación en \textit{Blender} de una parte de Ciudad Real a partir de sus datos \acs{OSM}.

\begin{figure}[!h]
\centering
   \includegraphics[width=13.5cm]{figs/blender-osm.png}
\caption{Una parte de Ciudad Real representada en \textit{Blender} a partir de sus datos \acs{OSM}}
\label{blender-osm}
\end{figure}

Para el manejo de todo lo relacionado con \acl{OSM}, se ha escrito la clase \textit{ManagerOSM}, que contiene diferentes métodos explicados a continuación:

\begin{itemize}
\item \textbf{\textit{downloadOSMFile}}: se conecta al servidor de \textit{overpass.osm.rambler.ru} para descargar los datos \acs{OSM} de los objetos manejados en \acl{OSM} (edificios, vías, etc) que se encuentran en una parte de la superficie terrestre indicada mediante unas coordenadas de la forma longitudMínima, latitudMínima, longitudMáxima, latitudMáxima. Una vez descargados, almacena los datos en un fichero para que puedan ser utilizados posteriormente en \textit{Blender}.
\item \textbf{\textit{enableAddons}}: permite  acceder a la lista de \textit{plugins} de \textit{Blender} para consultar si uno concreto está habilitado y, si no lo estuviera, habilitarlo. Se ha utilizado para habilitar el \textit{plugin} antes indicado utilizado para cargar los datos descargados mediante el método anterior en \textit{Blender}. 
\end{itemize}

Por último, para un manual sobre la obtención de coordenadas, la descarga de un fichero .osm y la posterior representación en \textit{Blender} de los datos que contenga, y para habilitar un \textit{plugin} (ya que hay otra forma de hacerlo a parte de la ya explicada), consúltese Anexo \ref{uso-plataforma}.

\section{Virtualización de la plataforma}

Tras el desarrollo de las distintas partes de la plataforma GEVA explicadas anteriormente, se continuó con la automatización del despliegue de la plataforma por los motivos comentados en la subsección \ref{justificacion-virt}. Para ello, se decidió crear y configurar una máquina virtual de forma automática y que tuviera instalados los mismos paquetes, herramientas y librerías que las utilizadas durante el desarrollo del proyecto. 

Como ya se dijo en las subsecciones \ref{eleccion-vagrant}, \ref{soft-proveedor} y \ref{eleccion-ansible}, se ha escogido \textit{Vagrant} como software de creación y gestión de máquinas virtuales, \textit{VirtualBox} como software proveedor y \textit{Ansible} como software de aprovisionamiento de la máquina virtual.

Para la virtualización de la plataforma se ha escogido la ``caja'' \textit{ubuntu/xenial32}, descargada del repositorio de cajas de \textit{Hashicorp} y que establece un entorno de trabajo con Ubuntu 16.04 LTS de 32 bits. Una vez descargada, se ha escrito el archivo \textit{Vagrantfile} para configurar distintos aspectos de la máquina virtual: especificación de la caja a utilizar, indicar que utilice una red pública, que utilice como software de aprovisionamiento \textit{Ansible} y como \textit{playbook} el que he denominado \textit{playbook.yml} y, por último, que use como proveedor \textit{VirtualBox}.

Una vez establecida la configuración de la máquina virtual, era necesario indicar en el archivo \textit{playbook.yml} las tareas de instalación de las herramientas, librerías y paquetes necesarios. Además, también se han indicado las tareas de creación de directorios y copia de archivos de la máquina nativa a la máquina virtual. Las tareas realizadas se han dividido en cuatro grupos:

\begin{itemize}
\item Primer grupo: instalan las herramientas y librerías que van a utilizarse siendo superusuario.
\item Segundo grupo: comienzan la instalación de \textit{OpenCV} mediante la compilación del código fuente sin necesidad de ser superusuario.
\item Tercer grupo: terminan la compilación del código fuente de \textit{OpenCV} realizando algunas operaciones como superusuario.
\item Cuarto grupo: establecen la estructura de directorios explicada en la sección \ref{estruct-dir} del Anexo \ref{uso-plataforma}. Para ello, crean las carpetas necesarias y copian los archivos que se van a utilizar desde la máquina \textit{host} a la máquina virtual.
\end{itemize}

Puede verse un esquema de la virtualización de la plataforma en la figura \ref{virt-plataforma}. Cabe indicar que en dicha figura se muestra que los clientes y el servidor \textit{ZeroC Ice} se encuentran en la misma máquina virtual. Ésto es únicamente para comprobar la correcta virtualización de la plataforma. Si se tratase de un entorno \textit{cloud}, los clientes estarían distribuidos por una ciudad real, ya que como se ha dicho anteriormente, dichos clientes serían sensores, cámaras de vídeo, etc.

\begin{figure}[!h]
\centering
   \includegraphics[width=15cm]{figs/virt-plataforma.png}
\caption{Virtualización de la plataforma para su despliegue automático}
\label{virt-plataforma}
\end{figure}

Para consultar una guía del despliegue de la plataforma véase Anexo \ref{instalacion} y para una guía del uso de la plataforma consúltese el Anexo \ref{uso-plataforma}.


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
